#-WARNING: this file has been automatically generated do not edit
#-or all your changes will be lost!


require stream
require dln

epicsEnvSet("STREAM_PROTOCOL_PATH","${dln_DB}")

epicsEnvSet("DLN",     "DLN01")
epicsEnvSet("CHAN",    "A")
epicsEnvSet("AMC",     "03")
epicsEnvSet("IP_ADDR", "172.30.6.46")
epicsEnvSet("HASH",    "0xcd472185")

iocshLoad("$(dln_DIR)$(DLN).iocsh", "CHAN=$(CHAN), HASH=$(HASH), BUILDT=1709217494, DLN=$(DLN), P=FBIS-$(DLN), R=Ctrl-AMC-$(AMC), R1=Ctrl-EVR-01, BRD=$(CHAN), PORT=$(DLN)-$(CHAN), IP_ADDR=$(IP_ADDR):42000")

iocInit()
