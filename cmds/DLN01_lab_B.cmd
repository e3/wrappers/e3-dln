#-WARNING: this file has been automatically generated do not edit
#-or all your changes will be lost!


require stream
require dln

epicsEnvSet("STREAM_PROTOCOL_PATH","${dln_DB}")

epicsEnvSet("DLN",     "DLN02")
epicsEnvSet("CHAN",    "B")
epicsEnvSet("AMC",     "05")
epicsEnvSet("IP_ADDR", "172.30.6.103")
epicsEnvSet("HASH",    "0xdf12e4fa")

iocshLoad("$(dln_DIR)$(DLN).iocsh", "CHAN=$(CHAN), HASH=$(HASH), BUILDT=1234, DLN=$(DLN), P=FBIS-$(DLN), R=Ctrl-AMC-$(AMC), R1=Ctrl-EVR-01, BRD=$(CHAN), PORT=FBIS-$(DLN)-$(CHAN), IP_ADDR=$(IP_ADDR):42000")

iocInit()
