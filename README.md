# e3-dln

This module is responible for monitoring and controlling the MPS DLNs.

For more information, see [docs/README.md](docs/README.md).

## Prerequisites

The following section describe the necessary steps required to use the build system.

###  Clone (Initial Checkout)
The e3-dln makes use of git-submodules for custom iocsh files and to generate raw PVs
(PVs to access the daemon). Use the following instruction for the initial checkout.

```console
# initial checkout
git submodule update --init --recursive
```
Note: Submodules are not updated automatically.

### Update the submodule
```
git submodule update --remote --merge --force
```
## Python scripts

In the folder *tools/* there are python scripts needed to generate
* DLNxx.iocsh files (in the folder *iocsh/*)
* DLNxx_regs.template files (in the folder *dln/Db/*)
* DLNxxA.archive files (in the folder *tools/to_archive*)

```console
cd tools
# example for DLN01
python3 create_iocsh_file.py DLN01
python3 create_specific_PVs_DLNxx.py DLN01
python3 pvs_to_archive.py DLN01 A
```
In the folder *tools/* there is also a bash script that iterate the python
scripts according to the *.ini* files in the folder *tools/conf*

```console
cd tools
bash iterate_python_scripts
```

## Installation

```sh
$ make init patch build
$ sudo make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh.bash -r dln
```
