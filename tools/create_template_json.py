import sys
from pathlib import Path
import pandas as pd
import json


class ParametrizationToJson:
    def __init__(self, dln):
        """Initialize with Excel file and prepare dataframes."""
        param_folder_path = Path("conf/Parameterization_Files_Excel/")
        assert (
            param_folder_path.exists()
        ), f"Folder {param_folder_path} does not exist, please sync the submodule"

        dln_file = param_folder_path / f"{dln}.xlsm"
        self.dln_df = pd.read_excel(
            dln_file, engine="openpyxl", sheet_name="DLN_PU"
        ).reset_index()
        self.dln_links = pd.read_excel(
            dln_file, engine="openpyxl", sheet_name="DLN_BASIC_SETUP"
        ).reset_index()
        self.dln_links.columns = self.dln_links.columns.str.strip()

        # Column references
        self.C_SPU = "DLN PU Parametrization"
        self.C_Concatenation = "Unnamed: 12"
        self.C_ALIAS = "Unnamed: 13"
        self.C_ass_interface = next(
            col for col in self.dln_links.columns if "Unnamed" in col and "7" in col
        )
        # Data arrays
        self.spu, self.sspu, self.pbd, self.pbm, self.slink = [], [], [], [], []

    def __build_json_dict(self):
        """Build JSON structure from the data."""
        i_spu, i_sspu, i_pbd, i_pbm = 0, 0, 0, 0
        for _, row in self.dln_df.iterrows():
            pu = row[self.C_SPU]
            if pu in ["spu", "sis_spu"]:
                self.spu.append(
                    f'\t\t{{"id": "{i_spu}", "label": "{row[self.C_Concatenation]}", "prp": "{row[self.C_ALIAS]}"}}'
                )
                i_spu += 1
            elif pu in ["sspu", "sis_sspu"]:
                self.sspu.append(
                    f'\t\t{{"id": "{i_sspu}", "label": "{row[self.C_Concatenation]}", "prp": "{row[self.C_ALIAS]}"}}'
                )
                i_sspu += 1
            elif pu in ["pbdspu", "sis_pbdspu"]:
                self.pbd.append(
                    f'\t\t{{"id": "{i_pbd}", "label": "{row[self.C_Concatenation]}", "prp": "{row[self.C_ALIAS]}"}}'
                )
                i_pbd += 1
            elif pu in ["pbmspu", "sis_pbmspu"]:
                self.pbm.append(
                    f'\t\t{{"id": "{i_pbm}", "label": "{row[self.C_Concatenation]}", "prp": "{row[self.C_ALIAS]}"}}'
                )
                i_pbm += 1

    def get_slink(self):
        """Extract slink data."""
        i = 0
        for index, row in self.dln_links.iterrows():
            if 5 <= index <= 11 and row["Unnamed: 2"] != 0:
                # Make sure the column exists
                if self.C_ass_interface in row:
                    self.slink.append(
                        f'\t\t{{"id": "{i}", "label": "{row[self.C_ass_interface]}"}}'
                    )
                    i += 1
                else:
                    print(
                        f"Column {self.C_ass_interface} not found in row {index}. Skipping..."
                    )

    def get_json(self):
        """! It builds the json file as a string. Mainly it
        takes all items previously appended in the array members.
        """

        self.__build_json_dict()
        self.get_slink()
        j = "{\n"
        j += '  "st.cmd" : {\n'
        dln_instance = input("Enter value for dln_instance: ")
        dln_channel = input("Enter value for dln_channel: ")
        amc_slot = input("Enter value for amc_slot: ")
        ip_address = input("Enter value for ip_address: ")
        fbis_module_git_hash = input("Enter value for fbis_module_git_hash : ")
        fbis_module_git_hash = input("Enter the value for firmware_build_time: ")

        cmd_dict = {
            "dln_instance": dln_instance,
            "dln_channel": dln_channel,
            "amc_slot": amc_slot,
            "ip_address": ip_address,
            "fbis_module_git_hash": fbis_module_git_hash,
            "firmware_build_time": fbis_module_git_hash,
        }
        j += ",\n".join([f'    "{key}": "{value}"' for key, value in cmd_dict.items()])
        j += ",\n"

        j += '\t"slinks" : [\n'
        j += "".join(
            [" " + str(i) + ",\n" for i in self.slink]
        )  # Add a newline after each item
        j = j.rstrip(",\n")  # Remove the last comma and newline
        j += "\n\t],\n"

        j += '\t"spus" : [\n'
        j += "".join(
            [str(i) + ",\n" for i in self.spu]
        )  # Add a newline after each item
        j = j.rstrip(",\n")  # Remove the last comma and newline
        j += "\n\t]"
        if self.sspu:
            j += ",\n"
            j += '   "sspus" : [\n'
            j += "".join(
                [str(i) + ",\n" for i in self.sspu]
            )  # Add a newline after each item
            j = j.rstrip(",\n")  # Remove the last comma and newline
            j += "\n\t]"
        if self.pbd:
            j += ",\n"
            j += '\t"pbdspus" : [\n'
            j += "".join(
                [str(i) + ",\n" for i in self.pbd]
            )  # Add a newline after each item
            j = j.rstrip(",\n")  # Remove the last comma and newline
            j += "\n\t]"
        if self.pbm:
            j += ",\n"
            j += '\t"pbmspus" : [\n'
            j += "".join(
                [str(i) + ",\n" for i in self.pbm]
            )  # Add a newline after each item
            j = j.rstrip(",\n")  # Remove the last comma and newline
            j += "\n\t]"
        sis_in_data = any(
            "SIS" in entry
            for lst in [self.pbm, self.spu, self.pbd, self.slink]
            for entry in lst
        )
        if sis_in_data:
            j += ',\n\t"sis" : [\n    {}\n\t]'
        j += "\n  }"
        j += "\n}"

        return j


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please provide the Excel file name.")
        sys.exit(1)

    # Define paths
    file_to_open = sys.argv[1]
    file_number = Path(file_to_open).stem
    file_to_write = f"{file_number}.json"

    # Process and write JSON
    with open(file_to_write, "w") as f:
        dln = ParametrizationToJson(file_to_open).get_json()
        f.write(dln)
