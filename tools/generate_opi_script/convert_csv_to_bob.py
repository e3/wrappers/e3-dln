from typing import Tuple, Dict, NewType, List
import xml.etree
import xml.etree.ElementInclude
import xml.etree.ElementTree as ET
import xml.dom.minidom
import xml.etree.ElementTree
import pandas as pd
from dataclasses import dataclass, field
from pathlib import Path

MacroName = NewType("MacroName", str)
MacroValue = NewType("MacroValue", str)

Color = NewType("Color", str)
ColorProperty = NewType("ColorProperty", str)
PropertyValue = NewType("PropertyValue", "str")
ColorAttr = NewType("ColorAttr", Tuple[Tuple[ColorProperty, PropertyValue]])


@dataclass
class DisplayTags:
    name: str = ""
    width: str = ""
    height: str = ""
    background_color: Tuple[Tuple[ColorProperty, PropertyValue]] = ()


@dataclass
class FixedWidgetTags:
    file: str = ""
    width: int = 0
    height: int = 0
    resize: int = 0
    x: int = 0


@dataclass
class VariableWidgetTags:
    y: str = "10"
    name: str = "Hello"
    macros: Tuple[Tuple[str, str]] = ()


def fix_widget_setting(widget_setting: FixedWidgetTags) -> callable:
    def make_widget(widget_data: VariableWidgetTags) -> ET:
        variable_tags = [
            attribute for attribute in widget_data.__dir__() if "__" not in attribute
        ]
        fixed_tags = [
            attribute for attribute in widget_setting.__dir__() if "__" not in attribute
        ]

        def add_tag(
            widget: ET.Element, widget_data: VariableWidgetTags, attributes: List[str]
        ) -> ET.Element:
            if attributes == []:
                return widget
            elif type(getattr(widget_data, attributes[0])) == tuple:
                macros = ET.SubElement(widget, attributes[0])
                getattr(widget_data, attributes[0])
                for macroname, macrovalue in getattr(widget_data, attributes[0]):
                    ET.SubElement(macros, macroname).text = macrovalue
                return add_tag(widget, widget_data, attributes[1:])
            else:
                ET.SubElement(widget, attributes[0]).text = getattr(
                    widget_data, attributes[0]
                )
                return add_tag(widget, widget_data, attributes[1:])

        empty_widget = ET.Element("widget", {"type": "embedded", "version": "2.0.0"})
        return add_tag(
            add_tag(empty_widget, widget_setting, fixed_tags),
            widget_data,
            variable_tags,
        )

    return make_widget


def make_display(display_data: DisplayTags) -> ET.Element:
    tags = [attribute for attribute in display_data.__dir__() if "__" not in attribute]

    def add_tag(
        display: ET.Element, display_data: VariableWidgetTags, attributes: List[str]
    ) -> ET.Element:
        if attributes == []:
            return display
        elif type(getattr(display_data, attributes[0])) == tuple:
            color = ET.SubElement(display, attributes[0])
            getattr(display_data, attributes[0])
            for colorproperty, propertyvalue in getattr(display_data, attributes[0]):
                color.set(colorproperty, propertyvalue)
            return add_tag(display, display_data, attributes[1:])
        else:
            ET.SubElement(display, attributes[0]).text = getattr(
                display_data, attributes[0]
            )
            return add_tag(display, display_data, attributes[1:])

    return add_tag(ET.Element("display", {"version": "2.0.0"}), display_data, tags)


def make_pu_list(
    df: pd.DataFrame, display_setting: DisplayTags, widget_setting: FixedWidgetTags
) -> ET.Element:
    y = 10
    name = "Embedded Display_{i}"
    display = make_display(display_setting)
    make_widget = fix_widget_setting(widget_setting)
    for _, row in df.iterrows():
        macros = (("ID", str(row["PU Index"])), ("PRP", str(row["Alias"])))
        widget_data = VariableWidgetTags(
            y=str(y), name=name.format(i=row["PU Index"]), macros=macros
        )
        display.append(make_widget(widget_data))
        y += 30
    return display


def make_pu_scrolling_list(dln: str, pu_type: str) -> str:
    xml_templates = __import__("xml_templates.scrolling_list_template")
    scrolling_list_template = getattr(xml_templates, "scrolling_list_template")
    template = (
        getattr(scrolling_list_template, f"scrolling_list_{pu_type}_template")
    ).format(dln=dln)
    return template


def make_dln_xml(dln: str) -> str:
    from xml_templates.dln_template import dln_template

    return dln_template.format(DLN=dln.upper(), dln=dln.lower())


def make_dln_overview(dln: str) -> str:
    from xml_templates.dln_overview_template import dln_overview_template

    return dln_overview_template.format(
        DLN=dln.upper(), dln=dln.lower(), DLN_space=dln.replace("n", "n ").upper()
    )


def convert_csv_to_bob(csv_file_path: Path, save_dir: Path) -> None:
    df = pd.read_csv(str(csv_file_path))

    filters = ["spu", "sspu", "pbdspu", "pbmspu"]
    dln = df["DLN"][0]
    display_names = [f"{dln.lower()}_list_{pu}" for pu in filters]
    display_width = "760"
    display_height = "2360"
    display_backround_color = (
        ("red", "220"),
        ("green", "225"),
        ("blue", "221"),
        ("alpha", "0"),
    )
    display_data = [
        DisplayTags(
            display_name, display_width, display_height, display_backround_color
        )
        for display_name in display_names
    ]

    for i, pu in enumerate(filters):
        widget_setting = FixedWidgetTags(
            file=f"../../shared/PUs/{pu}_widget.bob",
            x="5",
            width="200",
            height="2000",
            resize="2",
        )
        display = make_pu_list(
            df[(df["PU Type"] == pu) | (df["PU Type"] == f"sis_{pu}")],
            display_data[i],
            widget_setting,
        )

        make_full_file_path = lambda file_name: str(save_dir / file_name)
        xml.etree.ElementTree.indent(display)
        with open(make_full_file_path(f"{display_names[i]}.bob"), "w") as file:
            file.write((xml.etree.ElementTree.tostring(display)).decode("us-ascii"))

        scrolling_list = make_pu_scrolling_list(dln.lower(), pu)
        with open(
            make_full_file_path(f"{dln.lower()}_list_{pu}_scrolling.bob"), "w"
        ) as file:
            file.write(scrolling_list)

        dln_xml = make_dln_xml(dln)
        with open(make_full_file_path(f"{dln.lower()}.bob"), "w") as file:
            file.write(dln_xml)

        dln_overview = make_dln_overview(dln)
        with open(make_full_file_path(f"{dln.lower()}_overview.bob"), "w") as file:
            file.write(dln_overview)
    return None


if __name__ == "__main__":
    # from pathlib import Path
    # data = Path("../csv/DLN01.csv")
    # df = pd.read_csv(str(data))

    # filters = ["spu","sspu","pbdspu","pbmspu"]
    # dln = df["DLN"][0]
    # display_names = [f"{dln.lower()}_list_{pu}" for pu in filters]
    # display_width = "760"
    # display_height = "2360"
    # display_backround_color = (("red","220"),("green","225"),("blue","221"),("alpha","0"))
    # display_data = [DisplayTags(display_name,display_width,display_height,display_backround_color) for display_name in display_names]

    # for i,pu in enumerate(filters):
    #     widget_setting = FixedWidgetTags(file=f"../../shared/PUs/{pu}_widget.bob",x="5",width="200",height="2000",resize="2")
    #     display = make_pu_list(
    #        df[(df["PU Type"]==pu) | (df["PU Type"]==f"sis_{pu}")],
    #        display_data[i],
    #        widget_setting
    #     )

    #     xml.etree.ElementTree.indent(display)
    #     with open(f"{display_names[i]}.xml","w") as file:
    #         file.write((xml.etree.ElementTree.tostring(display)).decode("us-ascii"))

    #     scrolling_list = make_pu_scrolling_list(dln.lower(),pu)
    #     with open(f"{dln.lower()}_list_{pu}_scrolling.bob",'w') as file:
    #         file.write(scrolling_list)

    #     dln_xml = make_dln_xml(dln)
    #     with open(f"{dln.lower()}.bob",'w') as file:
    #         file.write(dln_xml)

    #     dln_overview = make_dln_overview(dln)
    #     with open(f"{dln.lower()}_overview.bob",'w') as file:
    #         file.write(dln_overview)
    convert_csv_to_bob("csv/DLN01.csv")
