# Goals
The goal of this script is to generate the DLN OPI from the parametrization files.
# Running the script
## Initializing the submodules
Before running the script make sure that all the submodules have been initialized. This can be done
by going to the root of the repository and running
```
git submodule update --init --recursive
git submodule update --init --remote
```
## Running the script
Type
```
python3 main.py DLN
```
to generate the OPI for all the DLNs.

Type
```
python3 main.py DLNXX
```
to generate the OPI for a specific DLN.

## Updating the OPI
To update the OPI copy the newly generated directories in bob/ to systemexpert/99-shared/dln/DLN_instances/
