import sys
import os
from convert_ini_to_csv import convert_ini_to_csv
from convert_csv_to_bob import convert_csv_to_bob
from convert_xlsm_to_csv import convert_xlsm_to_csv
from pathlib import Path
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument("dln")
args = parser.parse_args()
param_folder_path = Path("../conf/Parameterization_Files_Excel/")
assert os.path.exists(
    str(param_folder_path)
), f"Directory {param_folder_path} does not exist"
save_dir = Path("bob")
if not save_dir.exists():
    save_dir.mkdir()

input_pattern = r"DLN\d\d"

if args.dln == "DLN":
    dlns = [f"DLN{i:02d}" for i in range(1, 8)]
    for dln in dlns:
        dln_dir = save_dir / f"{dln}"
        if not dln_dir.exists():
            dln_dir.mkdir()
        file_names_csv = Path(f"csv/{dln}.csv")
        file_names_xlsm = [str(param_folder_path / f"{dln}.xlsm")]
        convert_xlsm_to_csv(file_names_xlsm)
        convert_csv_to_bob(file_names_csv, dln_dir)
        print(f"Generated OPI for {dln} at {str(dln_dir)}")
elif re.match(input_pattern, args.dln):
    file_names_csv = Path(f"csv/{args.dln}.csv")
    file_names_xlsm = [str(param_folder_path / f"{args.dln}.xlsm")]
    dln_dir = save_dir / f"{args.dln}"
    if not dln_dir.exists():
        dln_dir.mkdir()
    convert_xlsm_to_csv(file_names_xlsm)
    convert_csv_to_bob(file_names_csv, save_dir)
    print(f"Generated OPI for {args.dln} at {str(dln_dir)}")
else:
    assert False, r"Bad input expected DLN or DLN\d\d"
