spu_starter = """<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>{dln}_list_{list_type}</name>
  <width>760</width>
  <height>2360</height>
  <background_color>
    <color red="220" green="225" blue="221" alpha="0">
    </color>
  </background_color>
  """
spu_widget = """<widget type="embedded" version="2.0.0">
    <name>Embedded Display_{embedded_display_index}</name>
    <macros>
      <ID>{PV_ID}</ID>
      <PRP>{PRP}</PRP>
    </macros>
    <file>../../shared/PUs/{list_type}_widget.bob</file>
    <x>13</x>
    <y>{y_coordinate}</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  """
spu_ender = """</display>\n"""
