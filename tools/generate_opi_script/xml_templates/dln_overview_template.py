dln_overview_template = """<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>$(P) Overview</name>
  <macros>
    <P>FBIS-{DLN}:</P>
  </macros>
  <width>1820</width>
  <height>1037</height>
  <grid_visible>false</grid_visible>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_13</name>
    <width>75</width>
    <height>200</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_14</name>
    <macros>
      <BRD>A</BRD>
      <R>Ctrl-AMC-03:</R>
    </macros>
    <file>{dln}.bob</file>
    <x>20</x>
    <y>70</y>
    <width>880</width>
    <height>947</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display_15</name>
    <macros>
      <BRD>B</BRD>
      <R>Ctrl-AMC-05:</R>
    </macros>
    <file>{dln}.bob</file>
    <x>920</x>
    <y>70</y>
    <width>880</width>
    <height>947</height>
    <resize>1</resize>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_47</name>
    <width>1820</width>
    <height>50</height>
    <line_width>0</line_width>
    <background_color>
      <color red="224" green="105" blue="158">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>TITLE</class>
    <text>FBIS {DLN_space}</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>590</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_58</name>
    <text>System Expert Screen</text>
    <x>1590</x>
    <y>18</y>
    <width>210</width>
    <height>30</height>
    <font>
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>2</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
</display>
"""
