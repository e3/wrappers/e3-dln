import pandas as pd
import os


def get_dln(parametrization_file_name: str):
    split_slash = parametrization_file_name.split(r"/")
    dln_file = split_slash[-1]
    dln = dln_file[:5]
    return dln


file_names = ["ESS-3439001 - DLN 01 Parameterization file.xlsm"]


def convert_xlsm_to_csv(file_names: list):
    if os.path.exists("csv"):
        pass
    else:
        os.mkdir("csv")

    for file_name in file_names:
        # file_name = "DLN01.ini"
        sheet_name = "DLN_PU"
        path = file_name
        df = pd.read_excel(path, sheet_name=sheet_name, skiprows=5, engine="openpyxl")
        selected_columns = ["PU Type", "Alias"]
        subtable = df[selected_columns]
        dln = get_dln(file_name)
        spu_index = 0
        sspu_index = 0
        pbdspu_index = 0
        pbmspu_index = 0

        with open("csv/{}.csv".format(dln), "w") as file:
            file.write("DLN,PU Type,PU Index,Alias\n")

            for index, row in subtable.iterrows():
                pu_type = row["PU Type"]
                alias = row["Alias"]
                if pu_type == "spu" or pu_type == "sis_spu":
                    file.write("{},{},{},{}\n".format(dln, pu_type, spu_index, alias))
                    spu_index += 1
                elif pu_type == "sspu":
                    file.write("{},{},{},{}\n".format(dln, pu_type, sspu_index, alias))
                    sspu_index += 1
                elif pu_type == "pbdspu" or pu_type == "sis_pbdspu":
                    file.write(
                        "{},{},{},{}\n".format(dln, pu_type, pbdspu_index, alias)
                    )
                    pbdspu_index += 1
                elif pu_type == "pbmspu" or pu_type == "sis_pbmspu":
                    file.write(
                        "{},{},{},{}\n".format(dln, pu_type, pbmspu_index, alias)
                    )
                    pbmspu_index += 1
                elif type(pu_type) == float:
                    pass
                else:
                    assert False, f"Case {pu_type} not handled."
