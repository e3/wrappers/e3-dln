def convert_ini_to_csv(file_names: list):
    for file_name in file_names:
        # file_name = "DLN01.ini"
        path = "ini/" + file_name

        def get_dln_name(file_name: str):
            split_dot = file_name.split(".")
            return split_dot[0]

        def get_index(title: str):
            index = ""
            for char in title:
                if char.isdigit():
                    index = index + char
            return index

        def get_type(title: str):
            title_type = ""
            for char in title:
                if not char.isdigit():
                    title_type = title_type + char
            return title_type

        initialization_file = open(path, "r")
        data = initialization_file.readlines()
        dln = get_dln_name(file_name)
        with open("csv/{}.csv".format(dln), "w") as file:
            file.write("DLN,title,title_type,title_index,alias\n")

            for line_index in range(len(data)):
                find_open_square_bracket = data[line_index].find("[")
                find_alias = data[line_index].find("(Alias")
                if find_open_square_bracket != -1:
                    find_close_square_bracket = data[line_index].find("]")
                    next_line_index = line_index + 1
                    find_alias = data[next_line_index].find("(Alias")
                    find_close_bracket = data[next_line_index].find(")")

                    title = data[line_index][
                        find_open_square_bracket + 1 : find_close_square_bracket
                    ]
                    alias = data[next_line_index][find_alias + 8 : find_close_bracket]
                    index = get_index(title)
                    title_type = get_type(title)
                    dln = get_dln_name(file_name)
                    file.write(
                        "{},{},{},{},{}\n".format(dln, title, title_type, index, alias)
                    )
