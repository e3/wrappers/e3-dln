#!/usr/bin/python3

import sys
from pathlib import Path

global str

if len(sys.argv) < 2:
    print("Insert DLN name, i.e DLN01")
    exit(1)

folder_to_open = Path("conf/DLN/")
file_to_open = folder_to_open / "".join(sys.argv[1] + ".ini")

folder_to_write = Path("../dln/Db/")
file_to_write = folder_to_write / "".join(sys.argv[1] + "_PVs" + ".template")

f = open(file_to_open, "r")
lines = f.readlines()
f.close()

disclaimer = "#- WARNING: this file has been automatically generated, \
do not edit\n#- or all your changes will be lost!\n"

# CS (Chip Select) for DLN register paging

C_DLN_CS_CFG = 0  # DLN global regsiter chip-select base
C_DLN_CS_HW = 1  # HW register chip-select base
C_DLN_CS_TEST = 16  # DLN Test register chip-select base
C_DLN_CS_OPL = 64  # DLN OPL register chip-select base
C_DLN_CS_SLINK = 512  # SLink register chip-select base
C_DLN_CS_SPU = 1024  # DLN signal processing units chip-select base
C_DLN_CS_PBMSPU = 2048  # DLN PBM signal processing units chip-select base
C_DLN_CS_PBDSPU = 3072  # DLN PBD signal processing units chip-select base
C_DLN_CS_EBISPU = 4096  # DLN EBI signal processing units chip-select base
C_DLN_CS_SSPU = 8192  # DLN state supervision units chip-select base
C_DLN_CS_POS = 9999  # To read the number of PUs that can cause a BI/RBI/EBI
# (for POS)

cs_values = [
    C_DLN_CS_CFG,
    C_DLN_CS_HW,
    C_DLN_CS_TEST,
    C_DLN_CS_OPL,
    C_DLN_CS_SLINK,
    C_DLN_CS_SPU,
    C_DLN_CS_PBMSPU,
    C_DLN_CS_PBDSPU,
    C_DLN_CS_EBISPU,
    C_DLN_CS_SSPU,
    C_DLN_CS_POS,
]

# The following values can change from DLN to DLN
# and from commissioning phase to phase
no_slink = 0
no_spu = 0
no_pbmspu = 0
no_pbdspu = 0
no_ebispu = 0
no_sspu = 0


for row in lines:
    if row.find("dln_spu") > -1 or row.find("dln_sis_spu") > -1:
        no_spu = no_spu + 1
    if row.find("dln_pbd") > -1 or row.find("dln_sis_pbdspu") > -1:
        no_pbdspu = no_pbdspu + 1
    if row.find("dln_pbm") > -1 or row.find("dln_sis_pbmspu") > -1:
        no_pbmspu = no_pbmspu + 1
    if row.find("dln_sspu") > -1:
        no_sspu = no_sspu + 1
    if row.find("dln_slink") > -1:
        no_slink = no_slink + 1


###########################################################
# The number of registers and their offset values MUST be
# aligned to what written in the DAOT, in the file daemon.c
# https://gitlab.esss.lu.se/ics-protection/fbis/fbis_epics/dln/daot/-/blob/master/src/daemon.c
###########################################################

# registers (offset) to be read just once, at the IOC boot.
cfg_addresses_synth = [
    0x0004,
    0x0008,
    0x000C,
    0x0010,
    0x0014,
    0x0018,
    0x001C,
    0x0030,
    0x40,
]
hw_addresses_synth = [0x0008, 0x000C, 0x0010, 0x0014, 0x0074, 0x0078, 0x007C]
test_addresses_synth = []
opl_addresses_synth = []
slink_addresses_synth = [0x0000]
spu_addresses_synth = [0x0004, 0x0008, 0x0014, 0x0018, 0x0020, 0x0034, 0x0038, 0x007C]
pbm_addresses_synth = [0x0004, 0x0008, 0x0014, 0x0018, 0x002C, 0x0030, 0x007C]
pbd_addresses_synth = [0x0004, 0x0008, 0x0014, 0x0018, 0x002C, 0x0030, 0x007C]
ebi_addresses_synth = [0x0004, 0x0008, 0x007C]
sspu_addresses_synth = [0x0004, 0x0008, 0x000C, 0x0010, 0x007C]

waveform_synth = 'record(waveform, "$(P)$(R)#synth_regs_$(BRD)")\
           \n{\n\
           field(DESC, "Regs fixed in the firmware")\n\
           field(DTYP, "stream")\n\
           field(INP,  "@dln.proto read_regs_synth\
(0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,%i) $(PORT)")\n\
           field(FTVL, "LONG")\n\
           field(NELM, "%i")\n\
           field(PINI, "1")\n}\n\n'

subArray_synth = 'record(subArray, "$(P)$(R)#r_%i_%i_0x%04x_$(BRD)")\
           \n{\n\
           \tfield(DESC, "Reg ID=%i CS=%i ADDR=0x%04x")\n\
           \tfield(INP,  "$(P)$(R)#synth_regs_$(BRD).VAL CPP")\n\
           \tfield(FTVL, "LONG")\n\
           \tfield(NELM, "1")\n\
           \tfield(MALM, "%i")\n\
           \tfield(INDX, "%i")\n}\n'


# registers (offset) to be read periodically.
cfg_addresses = [
    0x0000,
    0x0028,
    0x002C,
    0x0034,
    0x0038,
    0x0044,
    0x004C,
    0x0060,
    0x0064,
    0x0068,
    0x006C,
    0x0070,
    0x0074,
    0x0078,
    0x007C,
]
hw_addresses = [0x0004, 0x006C, 0x0070]
test_addresses = [0x0004, 0x0008, 0x000C]
opl_addresses = [
    0x0004,
    0x0008,
    0x000C,
    0x0010,
    0x0014,
    0x0018,
    0x001C,
    0x0020,
    0x0024,
    0x0028,
    0x002C,
    0x0030,
    0x0034,
    0x003C,
    0x0040,
    0x0044,
    0x0048,
    0x004C,
    0x0050,
    0x0054,
    0x0058,
    0x005C,
    0x0060,
    0x0064,
    0x0068,
    0x006C,
    0x0070,
    0x0074,
    0x0078,
]
slink_addresses = [
    0x0004,
    0x0008,
    0x000C,
    0x0010,
    0x0014,
    0x001C,
    0x0020,
    0x0024,
    0x0028,
    0x002C,
    0x0030,
    0x0034,
    0x0038,
    0x003C,
    0x0040,
    0x0044,
    0x0048,
    0x004C,
]
spu_addresses = [0x000C, 0x0010, 0x001C, 0x0024, 0x0028, 0x0030]
pbm_addresses = [0x000C, 0x0010, 0x001C, 0x0028]
pbd_addresses = [0x000C, 0x0010, 0x001C, 0x0028]
ebi_addresses = [0x000C]
sspu_addresses = [0x0014, 0x0018, 0x0020]
pos_addresses = [0x0001, 0x0002, 0x0003]  # BI,RBI and EBI PUs respectively


waveform = 'record(waveform, "$(P)$(R)#regs_from_hw_$(BRD)")\
           \n{\n\
           \tfield(DESC, "Regs to be read periodically")\n\
           \tfield(DTYP, "stream")\n\
           \tfield(INP,  "@dln.proto read_regs\
(0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,%i) $(PORT)")\n\
           \tfield(FTVL, "LONG")\n\
           \tfield(NELM, "%i")\n\
           \tfield(SCAN, "1 second")\n}\
           \n\nrecord(aSub,"$(P)$(R)#sub_regs_$(BRD)")\
           \n{\n\
           \tfield(DESC, "Check Alarm Severity of #regs_from_hw")\n\
           \tfield(SNAM, "sub_regs")\n\
           \tfield(INPA, "$(P)$(R)#regs_from_hw_$(BRD).VAL CPP")\n\
           \tfield(FTA,  "LONG")\n\
           \tfield(NOA,  "%i")\n\
           \tfield(INPB, "$(P)$(R)#regs_from_hw_$(BRD).SEVR")\n\
           \tfield(FTB,  "LONG")\n\
           \tfield(NOB,  "1")  \n\
           \tfield(FTVA, "LONG")\n\
           \tfield(NOVA, "%i")\n\
           \tfield(NEVA, "%i")\n\
           \tfield(OUTA, "$(P)$(R)#regs_$(BRD) PP")\n}\
           \n\nrecord(waveform, "$(P)$(R)#regs_$(BRD)")\n\
           \n{\n\
           \tfield(DESC, "Regs read periodically")\n\
           \tfield(INP,  "$(P)$(R)#sub_regs_$(BRD).VALA")\n\
           \tfield(FTVL, "LONG")\n\
           \tfield(NELM, "%i")\n}\n\n'

subArray = 'record(subArray, "$(P)$(R)#r_%i_%i_0x%04x_$(BRD)")\
           \n{\n\
           \tfield(DESC, "Reg ID=%i CS=%i ADDR=0x%04x")\n\
           \tfield(INP,  "$(P)$(R)#regs_$(BRD).VAL CPP")\n\
           \tfield(FTVL, "LONG")\n\
           \tfield(NELM, "1")\n\
           \tfield(MALM, "%i")\n\
           \tfield(INDX, "%i")\n}\n'


rearm = 'record(longout, "$(P)$(R)#rearm_$(BRD)")\
           \n{\n\
           \tfield(DESC, "Reset all latched inputs")\n\
           \tfield(DTYP,  "stream")\n\
           \tfield(OUT,  "@dln.proto rearm(0x%02x,0x%02x) $(PORT)")\n}\n'

delay_synth = '# To avoid initial clash with #regs_from_hw\
    \nrecord(calcout, "$(P)$(R)#delay_synth_regs_$(BRD)"){\n\
    field (CALC, "1")\n\
    field (OUT,  "$(P)$(R)#synth_regs_$(BRD) PP")\n\
    field (PINI, "RUNNING")\n\
    field (ODLY, "5") #seconds\n}\n'

lines = []


############################################################

i = 0
for cs in cs_values:
    if cs == C_DLN_CS_CFG:
        for addr in cfg_addresses_synth:
            i = i + 1
    if cs == C_DLN_CS_HW:
        for addr in hw_addresses_synth:
            i = i + 1
    if cs == C_DLN_CS_TEST:
        for addr in test_addresses_synth:
            i = i + 1
    if cs == C_DLN_CS_OPL:
        for addr in opl_addresses_synth:
            i = i + 1
    if cs == C_DLN_CS_SLINK:
        for inst in range(no_slink):
            for addr in slink_addresses_synth:
                i = i + 1
    if cs == C_DLN_CS_SPU:
        for inst in range(no_spu):
            for addr in spu_addresses_synth:
                i = i + 1
    if cs == C_DLN_CS_PBMSPU:
        for inst in range(no_pbmspu):
            for addr in pbm_addresses_synth:
                i = i + 1
    if cs == C_DLN_CS_PBDSPU:
        for inst in range(no_pbdspu):
            for addr in pbd_addresses_synth:
                i = i + 1
    if cs == C_DLN_CS_EBISPU:
        for inst in range(no_ebispu):
            for addr in ebi_addresses_synth:
                i = i + 1
    if cs == C_DLN_CS_SSPU:
        for inst in range(no_sspu):
            for addr in sspu_addresses_synth:
                i = i + 1

n_synth = i


i = 0
for cs in cs_values:
    if cs == C_DLN_CS_CFG:
        for addr in cfg_addresses_synth:
            lines.append(subArray_synth % (0, cs, addr, 0, cs, addr, n_synth, i))
            i = i + 1
    if cs == C_DLN_CS_HW:
        for addr in hw_addresses_synth:
            lines.append(subArray_synth % (0, cs, addr, 0, cs, addr, n_synth, i))
            i = i + 1
    if cs == C_DLN_CS_TEST:
        for addr in test_addresses_synth:
            lines.append(subArray_synth % (0, cs, addr, 0, cs, addr, n_synth, i))
            i = i + 1
    if cs == C_DLN_CS_OPL:
        for addr in opl_addresses_synth:
            lines.append(subArray_synth % (0, cs, addr, 0, cs, addr, n_synth, i))
            i = i + 1
    if cs == C_DLN_CS_SLINK:
        for inst in range(no_slink):
            for addr in slink_addresses_synth:
                lines.append(
                    subArray_synth % (inst, cs, addr, inst, cs, addr, n_synth, i)
                )
                i = i + 1
    if cs == C_DLN_CS_SPU:
        for inst in range(no_spu):
            for addr in spu_addresses_synth:
                lines.append(
                    subArray_synth % (inst, cs, addr, inst, cs, addr, n_synth, i)
                )
                i = i + 1
    if cs == C_DLN_CS_PBMSPU:
        for inst in range(no_pbmspu):
            for addr in pbm_addresses_synth:
                lines.append(
                    subArray_synth % (inst, cs, addr, inst, cs, addr, n_synth, i)
                )
                i = i + 1
    if cs == C_DLN_CS_PBDSPU:
        for inst in range(no_pbdspu):
            for addr in pbd_addresses_synth:
                lines.append(
                    subArray_synth % (inst, cs, addr, inst, cs, addr, n_synth, i)
                )
                i = i + 1
    if cs == C_DLN_CS_EBISPU:
        for inst in range(no_ebispu):
            for addr in ebi_addresses_synth:
                lines.append(
                    subArray_synth % (inst, cs, addr, inst, cs, addr, n_synth, i)
                )
                i = i + 1
    if cs == C_DLN_CS_SSPU:
        for inst in range(no_sspu):
            for addr in sspu_addresses_synth:
                lines.append(
                    subArray_synth % (inst, cs, addr, inst, cs, addr, n_synth, i)
                )
                i = i + 1


############################################################

i = 0
for cs in cs_values:
    if cs == C_DLN_CS_CFG:
        for addr in cfg_addresses:
            i = i + 1
    if cs == C_DLN_CS_HW:
        for addr in hw_addresses:
            i = i + 1
    if cs == C_DLN_CS_TEST:
        for addr in test_addresses:
            i = i + 1
    if cs == C_DLN_CS_OPL:
        for addr in opl_addresses:
            i = i + 1
    if cs == C_DLN_CS_SLINK:
        for inst in range(no_slink):
            for addr in slink_addresses:
                i = i + 1
    if cs == C_DLN_CS_SPU:
        for inst in range(no_spu):
            for addr in spu_addresses:
                i = i + 1
    if cs == C_DLN_CS_PBMSPU:
        for inst in range(no_pbmspu):
            for addr in pbm_addresses:
                i = i + 1
    if cs == C_DLN_CS_PBDSPU:
        for inst in range(no_pbdspu):
            for addr in pbd_addresses:
                i = i + 1
    if cs == C_DLN_CS_EBISPU:
        for inst in range(no_ebispu):
            for addr in ebi_addresses:
                i = i + 1
    if cs == C_DLN_CS_SSPU:
        for inst in range(no_sspu):
            for addr in sspu_addresses:
                i = i + 1
    if cs == C_DLN_CS_POS:
        for addr in pos_addresses:
            i = i + 1
n = i


i = 0
for cs in cs_values:
    if cs == C_DLN_CS_CFG:
        for addr in cfg_addresses:
            lines.append(subArray % (0, cs, addr, 0, cs, addr, n, i))
            i = i + 1
    if cs == C_DLN_CS_HW:
        for addr in hw_addresses:
            lines.append(subArray % (0, cs, addr, 0, cs, addr, n, i))
            i = i + 1
    if cs == C_DLN_CS_TEST:
        for addr in test_addresses:
            lines.append(subArray % (0, cs, addr, 0, cs, addr, n, i))
            i = i + 1
    if cs == C_DLN_CS_OPL:
        for addr in opl_addresses:
            lines.append(subArray % (0, cs, addr, 0, cs, addr, n, i))
            i = i + 1
    if cs == C_DLN_CS_SLINK:
        for inst in range(no_slink):
            for addr in slink_addresses:
                lines.append(subArray % (inst, cs, addr, inst, cs, addr, n, i))
                i = i + 1
    if cs == C_DLN_CS_SPU:
        for inst in range(no_spu):
            for addr in spu_addresses:
                lines.append(subArray % (inst, cs, addr, inst, cs, addr, n, i))
                i = i + 1
    if cs == C_DLN_CS_PBMSPU:
        for inst in range(no_pbmspu):
            for addr in pbm_addresses:
                lines.append(subArray % (inst, cs, addr, inst, cs, addr, n, i))
                i = i + 1
    if cs == C_DLN_CS_PBDSPU:
        for inst in range(no_pbdspu):
            for addr in pbd_addresses:
                lines.append(subArray % (inst, cs, addr, inst, cs, addr, n, i))
                i = i + 1
    if cs == C_DLN_CS_EBISPU:
        for inst in range(no_ebispu):
            for addr in ebi_addresses:
                lines.append(subArray % (inst, cs, addr, inst, cs, addr, n, i))
                i = i + 1
    if cs == C_DLN_CS_SSPU:
        for inst in range(no_sspu):
            for addr in sspu_addresses:
                lines.append(subArray % (inst, cs, addr, inst, cs, addr, n, i))
                i = i + 1
    if cs == C_DLN_CS_POS:
        for addr in pos_addresses:
            lines.append(subArray % (0, cs, addr, 0, cs, addr, n, i))
            i = i + 1

############################################################

rows = disclaimer
rows += "\n"
rows += rearm % (no_spu, no_sspu)
rows += "\n"
rows += delay_synth
rows += "\n"
rows += waveform_synth % (
    no_slink,
    no_spu,
    no_pbmspu,
    no_pbdspu,
    no_ebispu,
    no_sspu,
    4 * n_synth,
    n_synth,
)
rows += waveform % (
    no_slink,
    no_spu,
    no_pbmspu,
    no_pbdspu,
    no_ebispu,
    no_sspu,
    4 * n,
    n,
    n,
    n,
    n,
    n,
)

rows += "".join([str(i) for i in lines])
rows += "\n"

f = open(file_to_write, "w+")
f.write(rows)
f.close()
print("\n%s has been written.\n" % file_to_write)
