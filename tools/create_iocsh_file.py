#!/usr/bin/python3

import sys
from pathlib import Path

global str

if len(sys.argv) < 2:
    print("Insert DLN name, i.e. DLN01 ")
    exit(1)

folder_to_open = Path("conf/DLN/")
file_to_open = folder_to_open / "".join(sys.argv[1] + ".ini")

folder_to_write = Path("../iocsh/")
file_to_write = folder_to_write / "".join(sys.argv[1] + ".iocsh")

f = open(file_to_open, "r")
lines = f.readlines()
f.close()


disclaimer = "#- WARNING: this file has been automatically generated do not edit\n\
#- or all your changes will be lost!\n"

drvAsynIPPortConfigure = 'drvAsynIPPortConfigure("$(PORT)", \
    "$(IP_ADDR)", 0,0,1)\n'

dln_pvs = 'dbLoadRecords("$(DLN)_PVs.db", "PORT=$(PORT), \
       P=$(P):, R=$(R):, BRD=$(BRD)")\n'

cfg = 'dbLoadRecords("cfg.db",    "PORT=$(PORT), \
    P=$(P):, R=$(R):, R1=$(R1):, BRD=$(BRD), CFG_HASH=$(HASH), BUILDT=$(BUILDT)")'

hw = 'dbLoadRecords("hw.db",    "PORT=$(PORT), \
    P=$(P):, R=$(R):, BRD=$(BRD)")'

hb = 'dbLoadRecords("hb.db",    "_PORT=$(PORT), \
    _P=$(P):, _R=$(R):, _BRD=$(BRD)")'

opl = 'dbLoadRecords("opl.db",    "PORT=$(PORT), \
    P=$(P):, R=$(R):, BRD=$(BRD)")'

bso_cc = 'dbLoadRecords("bso_cc.db",    "PORT=$(PORT), \
    P=$(P):, R=$(R):, BRD=$(BRD)")'

pos = 'dbLoadRecords("pos.db",    "PORT=$(PORT), \
    P=$(P):, R=$(R):, BRD=$(BRD)")\n'

dbload = 'dbLoadRecords("%s.db",     "PORT=$(PORT), \
    P=$(P):, R=$(R):, BRD=$(BRD), ID=%i, LABEL=%s, PRP=%s")\n'

dbload_slink = 'dbLoadRecords("%s.db",     "PORT=$(PORT), \
    P=$(P):, R=$(R):, BRD=$(BRD), ID=%i, LABEL=%s")\n'

sis = 'dbLoadRecords("sis.db",    "PORT=$(PORT), \
    P=$(P):, R=$(R):, BRD=$(BRD)")\n'

list_cs = ["spu", "pbdspu", "pbmspu", "sspu", "slink"]

dbloads = []

# The following values can change from DLN to DLN
# and from commissioning phase to phase
spus = []
pbds = []
pbms = []
sspus = []
slinks = []
has_sis_signals = False  # variable to track SIS signals
next_line = False
id = 0
sub = ""
sub_old = ""


def analyze_row(row, index, next_line):

    id_ = int("".join(filter(str.isdigit, row)))
    sub_ = list_cs[index]
    next_line_ = not next_line
    return id_, sub_, next_line_


for row in lines:
    if row.strip():  # To not take into account blank lines
        if next_line is True:
            if sub == "slink":
                label = row[23:].strip()
                dbloads.append(dbload_slink % (sub, id, label))
            else:
                pvname = row[row.find("Alias: ") + 7 : row.find(")")]
                dbloads.append(dbload % (sub, id, row[14 : row.find(" (")], pvname))
            next_line = not next_line

        if row.find("dln_spu") > -1 or row.find("dln_sis_spu") > -1:
            row_analyzed = analyze_row(row, 0, next_line)
            print(row_analyzed)
            id = row_analyzed[0]
            sub = row_analyzed[1]
            next_line = row_analyzed[2]
        if row.find("dln_pbd") > -1 or row.find("dln_sis_pbdspu") > -1:
            row_analyzed = analyze_row(row, 1, next_line)
            id = row_analyzed[0]
            sub = row_analyzed[1]
            next_line = row_analyzed[2]
        if row.find("dln_pbm") > -1 or row.find("dln_sis_pbmspu") > -1:
            row_analyzed = analyze_row(row, 2, next_line)
            id = row_analyzed[0]
            sub = row_analyzed[1]
            next_line = row_analyzed[2]
        if row.find("dln_sspu") > -1:
            row_analyzed = analyze_row(row, 3, next_line)
            id = row_analyzed[0]
            sub = row_analyzed[1]
            next_line = row_analyzed[2]
        if row.find("dln_slink") > -1:
            row_analyzed = analyze_row(row, 4, next_line)
            id = row_analyzed[0]
            sub = row_analyzed[1]
            next_line = row_analyzed[2]
        if (
            row.find("dln_sis_spu") > -1
            or row.find("dln_sis_pbdspu") > -1
            or row.find("dln_sis_pbmspu") > -1
        ):
            has_sis_signals = True


cmd = disclaimer
cmd += "\n\n"
cmd += drvAsynIPPortConfigure
cmd += "\n"
cmd += dln_pvs
cmd += "\n"
cmd += cfg
cmd += "\n"
cmd += hw
cmd += "\n"
cmd += bso_cc
cmd += "\n"
cmd += opl
cmd += "\n"
cmd += "".join([str(i) for i in dbloads])
cmd += "\n"
cmd += pos
cmd += "\n"

if has_sis_signals:
    cmd += sis


f = open(file_to_write, "w")
f.write(cmd)
f.close()
print("\n%s has been written.\n" % file_to_write)
