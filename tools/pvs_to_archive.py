#!/usr/bin/python3

import sys
from pathlib import Path

global str

if len(sys.argv) < 3:
    print("Insert DLN name i.e. DLN01 A")
    exit(1)

folder_to_open = Path("../iocsh/")
file_to_open = folder_to_open / "".join(sys.argv[1] + ".iocsh")

folder_to_write = Path("to_archive/")
file_to_write = folder_to_write / "".join(sys.argv[1] + sys.argv[2] + ".archive")

db_folder = Path("../dln/Db/")


class File_db:
    def __init__(self, fld, name, P, R, idx, B, PRP):
        self.file = open(fld / "".join(name + ".db"), "r")
        self.P = P
        self.R = R
        self.idx = idx
        self.B = B
        self.PRP = PRP
        self.rows = []

    def parse_file(self):
        lines = self.file.readlines()

        for i in range(0, len(lines)):
            ll = lines[i]
            if ll.find("record(") > -1:
                name = ll[ll.find('"$') + 1 : ll.find(')"') + 1]  # name value
                if ll.find("ARCHIVE") > -1:
                    name = name.replace("$(P)", self.P)
                    name = name.replace("$(R)", self.R)
                    name = name.replace("$(ID)", self.idx)
                    name = name.replace("$(BRD)", self.B)
                    name = name.replace("$(PRP)", self.PRP)
                    self.rows.append(name)

        self.file.close()

    def get_rows(self):
        self.parse_file()
        return self.rows


def main():

    f = open(file_to_open, "r")
    lines = f.readlines()
    f.close()

    P = "FBIS-" + sys.argv[1] + ":"
    if sys.argv[2] == "A":
        R = "Ctrl-AMC-03:"
    elif sys.argv[2] == "B":
        R = "Ctrl-AMC-05:"
    else:
        print("Wrong argument. Choose among A and B")
        exit()

    db_instances = []

    for row in lines:
        if row.find("dbLoadRecords") > -1:
            _file = row[row.find("dbLoadRecords") + 15 : row.find(".db")]
            _file = _file.replace("$(DLN)", sys.argv[1])
            if row.find("ID") > -1:
                id = row[row.find("ID=") + 3 : row.find(", LABEL")]
                prp = row[row.find("PRP=") + 4 : row.find('")')]
            else:
                id = ""
                prp = ""
            db_instances.append(File_db(db_folder, _file, P, R, id, sys.argv[2], prp))

    f = open(file_to_write, "w")

    for db in db_instances:
        f.write("\n".join([str(i) for i in db.get_rows()]))
        f.write("\n")

    f.close()

    print("\n%s has been written. \n" % file_to_write)
    exit()


if __name__ == "__main__":
    main()
