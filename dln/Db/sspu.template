# Supervisor Signal Processing Unit (SSPU)
# ID        = ID the number of the SSPU                              0,1,2,3, ...
# 8192      = C_DLN_CS_SSPU
# 3rd arg   = index of the register.                                sspuid = 4, sspupp = 8, ...

#############################################################
# SPID
#############################################################
record(longin, "$(P)$(R)sspu_$(ID)_id_$(BRD)")
{
    field(DESC, "SSPU ID ")
    field(INP,  "$(P)$(R)#r_$(ID)_8192_0x0004_$(BRD).VAL CPP")
    field(FLNK, "$(PRP)_id_$(BRD) PP")
}
record(longin, "$(PRP)_id_$(BRD)")
{
    field (DESC, "SSPU id")
    field (INP,  "$(P)$(R)sspu_$(ID)_id_$(BRD)")
    info(aa_policy, "default")
    info(archiver, "tn")
}

record(stringin, "$(P)$(R)sspu_$(ID)_id_label_$(BRD)")
{
  field(DESC, "SSPU ID label ")
  field(VAL,  "$(LABEL)")
}

#############################################################
# State Supervision PU Param Register
#############################################################
record(longin, "$(P)$(R)#sspu_$(ID)_time_en_$(BRD)")
{
    field(DESC, "Timer span enable")
    field(INP,  "$(P)$(R)#r_$(ID)_8192_0x0008_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#sspu_$(ID)_timer_en_$(BRD)")
}

# Timer span enable
record(calcout, "$(P)$(R)#sspu_$(ID)_timer_en_$(BRD)")
{
    field (DESC, "Timer span enable")
    field (INPA, "$(P)$(R)#sspu_$(ID)_time_en_$(BRD).VAL")
    field (INPB, "0x00000008")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)sspu_$(ID)_times_en_$(BRD) PP")
}

record(bi, "$(P)$(R)sspu_$(ID)_times_en_$(BRD)")
{
    field (DESC, "Timer span enable")
    field (ZNAM, "Timespan not enable")
    field (ONAM, "Timespan enable")
}

#############################################################
# State Supervision PU Param TimeOut Register
#############################################################
record(longin, "$(P)$(R)#sspu_$(ID)_tmeout_$(BRD)")
{
    field(DESC, "Time out timer")
    field(INP,  "$(P)$(R)#r_$(ID)_8192_0x000c_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#sspu_$(ID)_tm_out_fo_$(BRD)")
}

record(fanout, "$(P)$(R)#sspu_$(ID)_tm_out_fo_$(BRD)")
{
    field(DESC, "Time out timer fanout")
    field(SELM, "All")
    field(LNK1, "$(P)$(R)#sspu_$(ID)_timeout_$(BRD)")
    field(LNK2, "$(P)$(R)#sspu_$(ID)_c_react_t_$(BRD)")
}

# Timeout timer
record(calcout, "$(P)$(R)#sspu_$(ID)_timeout_$(BRD)")
{
    field(DESC, "Time out timer")
    field(INPA, "$(P)$(R)#sspu_$(ID)_tmeout_$(BRD).VAL")
    field(INPB, "0x0fffffff")
    field(INPC, "8.192")
    field(CALC, "(A & B) * C")
    field(OUT,  "$(P)$(R)sspu_$(ID)_timeout_$(BRD) PP")
}

record(longin, "$(P)$(R)sspu_$(ID)_timeout_$(BRD)")
{
    field (DESC, "Time out timer")
    field (EGU, "ns")
    info(aa_policy, "default")
    info(archiver, "tn")
}



#############################################################
# State Supervision PU Param TimeSpan Register
#############################################################
record(longin, "$(P)$(R)#sspu_$(ID)_tmespan_$(BRD)")
{
    field(DESC, "Time span timer")
    field(INP,  "$(P)$(R)#r_$(ID)_8192_0x0010_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#sspu_$(ID)_timespan_$(BRD)")
}

# Timer span timer
record(calcout, "$(P)$(R)#sspu_$(ID)_timespan_$(BRD)")
{
    field(DESC, "Time span timer")
    field(INPA, "$(P)$(R)#sspu_$(ID)_tmespan_$(BRD).VAL")
    field(INPB, "0x0fffffff")
    field(INPC, "8.192")
    field(CALC, "(A & B) * C")
    field(OUT,  "$(P)$(R)sspu_$(ID)_timespan_$(BRD) PP")
}

record(longin, "$(P)$(R)sspu_$(ID)_timespan_$(BRD)")
{
    field (DESC, "Time span timer")
    field (EGU, "ns")
}

#############################################################
# State Supervision PU Status Word (obsolete TO CHECK !!!!)
#############################################################
record(longin, "$(P)$(R)#sspu_$(ID)_pu_stat_$(BRD)")
{
    field(DESC, "SSPU status")
    field(INP,  "$(P)$(R)#r_$(ID)_8192_0x0014_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#sspu_$(ID)_stat_fo_$(BRD)")
}

record(fanout, "$(P)$(R)#sspu_$(ID)_stat_fo_$(BRD)")
{
    field(DESC, "SSPU statut fanout")
    field(SELM, "All")
    field(LNK1, "$(P)$(R)#sspu_$(ID)_dlv_latch_$(BRD)")
    field(LNK2, "$(P)$(R)#sspu_$(ID)_dlv_ok_$(BRD)")
    field(LNK3, "$(P)$(R)#sspu_$(ID)_calc_in_$(BRD)")
    field(LNK4, "$(P)$(R)#sspu_$(ID)_clc_in_ok_$(BRD)")
    field(LNK5, "$(P)$(R)#sspu_$(ID)_tout_flg_$(BRD)")
    field(LNK6, "$(P)$(R)#sspu_$(ID)_tspan_flg_$(BRD)")
    field(LNK7, "$(P)$(R)#sspu_$(ID)_bso_flg_$(BRD)")
    field(LNK8, "$(P)$(R)#sspu_$(ID)_locked_$(BRD)")
}

record(calcout, "$(P)$(R)#sspu_$(ID)_calc_in_$(BRD)")
{
    field(DESC, "Calc val SSPU input")
    field(INPA, "$(P)$(R)#sspu_$(ID)_pu_stat_$(BRD).VAL")
    field(INPB, "0x000000c0")
    field(CALC, "(A & B) >> 6")
    field(OUT,  "$(P)$(R)sspu_$(ID)_in_$(BRD) PP")
}

# Signal processing unit's OKNOK input signal
record(mbbi, "$(P)$(R)sspu_$(ID)_in_$(BRD)")
{
    field (DESC, "Val SSPU input")
    field (INP,  "$(P)$(R)#sspu_$(ID)_calc_in_$(BRD)")
    field (ZRST, "ERR")
    field (ONST, "NOK")
    field (TWST, "OK")
    field (THST, "ERR")
    field (FLNK, "$(PRP)_in_$(BRD) PP")
}
record(mbbi, "$(PRP)_in_$(BRD)")
{
    field (DESC, "Val SSPU input")
    field (INP,  "$(P)$(R)sspu_$(ID)_in_$(BRD)")
    field (ZRST, "ERR")
    field (ONST, "NOK")
    field (TWST, "OK")
    field (THST, "ERR")
    info(aa_policy, "default")
    info(archiver, "tn")
}

record(calcout, "$(P)$(R)#sspu_$(ID)_clc_in_ok_$(BRD)")
{
    field(DESC, "Calc val SPU input")
    field(INPA, "$(P)$(R)#sspu_$(ID)_pu_stat_$(BRD).VAL")
    field(INPB, "0x000000c0")
    field(CALC, "((A & B) >> 6) = 2 ? 1 : 0")
    field(OUT,  "$(P)$(R)sspu_$(ID)_in_ok_$(BRD) PP")
}

record(bi, "$(P)$(R)sspu_$(ID)_in_ok_$(BRD)")
{
    alias("$(PRP)_in_ok_$(BRD)")
    field (DESC, "Is the feedback ok?")
    field (ZNAM, "Input not OK or ERR")
    field (ONAM, "Input OK")
    field (ZSV,  "MINOR")
    field (OSV,  "NO_ALARM")
}

#DLV.latched
record(calcout, "$(P)$(R)#sspu_$(ID)_dlv_latch_$(BRD)")
{
    field (DESC, "DLV latched")
    field (INPA, "$(P)$(R)#sspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000020")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)sspu_$(ID)_dlv_latch_$(BRD) PP")
}

record(bi, "$(P)$(R)sspu_$(ID)_dlv_latch_$(BRD)")
{
    field (DESC, "DLV latched")
    field (ZNAM, "DLN not latched")
    field (ONAM, "DLV latched")
    field (ZSV,  "NO_ALARM")
    field (OSV,  "MAJOR")
    field (FLNK, "$(PRP)_ltch_$(BRD) PP")
}
record(bi, "$(PRP)_ltch_$(BRD)")
{
    field (DESC, "DLV latched")
    field (INP,  "$(P)$(R)sspu_$(ID)_dlv_latch_$(BRD)")
    field (ZNAM, "DLN not latched")
    field (ONAM, "DLV latched")
    info(aa_policy, "default")
    info(archiver, "tn")
}

#DLV.ok
record(calcout, "$(P)$(R)#sspu_$(ID)_dlv_ok_$(BRD)")
{
    field (DESC, "DLV OK")
    field (INPA, "$(P)$(R)#sspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000010")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)sspu_$(ID)_dlv_ok_$(BRD) PP")
}

record(bi, "$(P)$(R)sspu_$(ID)_dlv_ok_$(BRD)")
{
    field (DESC, "DLV OK")
    field (ZNAM, "DLN NOK")
    field (ONAM, "DLV OK")
    field (ZSV,  "MAJOR")
    field (OSV,  "NO_ALARM")
    field (FLNK, "$(PRP)_$(BRD) PP")
}
record(bi, "$(PRP)_$(BRD)")
{
    field (DESC, "DLV OK")
    field (INP,  "$(P)$(R)sspu_$(ID)_dlv_ok_$(BRD)")
    field (ZNAM, "DLN NOK")
    field (ONAM, "DLV OK")
    field (ZSV,  "MAJOR")
    field (OSV,  "NO_ALARM")
    info(aa_policy, "default")
    info(archiver, "tn")
}

#DLV.latched
record(calcout, "$(P)$(R)#sspu_$(ID)_locked_$(BRD)")
{
    field (DESC, "Escalation request")
    field (INPA, "$(P)$(R)#sspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000008")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)sspu_$(ID)_locked_$(BRD) PP")
}

record(bi, "$(P)$(R)sspu_$(ID)_locked_$(BRD)")
{
    field (DESC, "Escalation request")
    field (ZNAM, "Not request")
    field (ONAM, "Request")
    field (ZSV,  "NO_ALARM")
    field (OSV,  "MAJOR")
}

#Timeout Timer run flag
record(calcout, "$(P)$(R)#sspu_$(ID)_tout_flg_$(BRD)")
{
    field (DESC, "Timeout flag")
    field (INPA, "$(P)$(R)#sspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000004")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)sspu_$(ID)_tout_flg_$(BRD) PP")
}

record(bi, "$(P)$(R)sspu_$(ID)_tout_flg_$(BRD)")
{
    field (DESC, "Timeout flag")
}

#Timespan Timer run flag
record(calcout, "$(P)$(R)#sspu_$(ID)_tspan_flg_$(BRD)")
{
    field (DESC, "Timespan flag")
    field (INPA, "$(P)$(R)#sspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000002")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)sspu_$(ID)_tspan_flg_$(BRD) PP")
}

record(bi, "$(P)$(R)sspu_$(ID)_tspan_flg_$(BRD)")
{
    field (DESC, "Timespan flag")
}

#Switch off request flag
record(calcout, "$(P)$(R)#sspu_$(ID)_bso_flg_$(BRD)")
{
    field (DESC, "Switch off request flag")
    field (INPA, "$(P)$(R)#sspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000001")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)sspu_$(ID)_bso_flg_$(BRD) PP")
}

record(bi, "$(P)$(R)sspu_$(ID)_bso_flg_$(BRD)")
{
    field (DESC, "Switch off request flag")
    field (ZNAM, "No switch off req")
    field (ONAM, "Switch off request")
    field (ZSV,  "NO_ALARM")
    field (OSV,  "MAJOR")
}

#############################################################
# Signal Processing Latch Reset Register
#############################################################
record(bo, "$(P)$(R)sspu_$(ID)_latch_rst_$(BRD)")
{
    field (DESC, "Reset Latch")
    field (DTYP, "Soft Channel")
    field (FLNK, "$(P)$(R)#sspu_$(ID)_ltc_rst_fo_$(BRD)")
    field (ONAM, "1")
    field (ZNAM, "0")
    field (PINI, 0)
}

record(fanout, "$(P)$(R)#sspu_$(ID)_ltc_rst_fo_$(BRD)")
{
    field(DESC, "Reset latch fanout")
    field(SELM, "All")
    field(LNK1, "$(P)$(R)#sspu_$(ID)_c_ltch_rst_$(BRD)")
    field(LNK2, "$(P)$(R)#sspu_$(ID)_ltc_rst_0_$(BRD)")
}

record(calcout, "$(P)$(R)#sspu_$(ID)_c_ltch_rst_$(BRD)")
{
    field (DESC, "Reset Latch")
    field (INPA, "$(P)$(R)sspu_$(ID)_latch_rst_$(BRD)")
    field (CALC, "A  = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)#sspu_$(ID)_latch_rst_$(BRD) PP")
    field (OOPT, "On Change")
}

record (calcout, "$(P)$(R)#sspu_$(ID)_ltc_rst_0_$(BRD)") {
    field (DESC, "Automatically reset value to 0")
    field (INPA, "$(P)$(R)sspu_$(ID)_latch_rst_$(BRD).VAL CPP")
    field (INPB, "$(P)$(R)sspu_$(ID)_latch_rst_$(BRD).RVAL")
    field (CALC, "A=1 && B = 0 ? 1 : 0")
    field (OUT,  "$(P)$(R)sspu_$(ID)_latch_rst_$(BRD).VAL PP")
    field (ODLY, 1)
}

record(longout, "$(P)$(R)#sspu_$(ID)_latch_rst_$(BRD)")
{
    field (DESC, "Reset Latch")
    field (DTYP, "stream")
    field (OUT,  "@dln.proto write(0x20,$(ID),0x11,0x9c) $(PORT)")
}

#############################################################
# State Supervision Timeout Status Register
#############################################################
record(longin, "$(P)$(R)#sspu_$(ID)_react_t_$(BRD)")
{
    field(DESC, "Previous Reaction Time")
    field(INP,  "$(P)$(R)#r_$(ID)_8192_0x0020_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#sspu_$(ID)_c_react_t_$(BRD)")
}

record(calcout, "$(P)$(R)#sspu_$(ID)_c_react_t_$(BRD)")
{
    field(DESC, "Previous Reaction Time")
    field(INPA, "$(P)$(R)#sspu_$(ID)_react_t_$(BRD).VAL CPP")
    field(INPB, "0x0fffffff")
    field(INPC, "$(P)$(R)#sspu_$(ID)_tmeout_$(BRD).VAL")
    field(CALC, "((C - A) & B) * 8.192")
    field(OUT,  "$(P)$(R)sspu_$(ID)_react_t_$(BRD) PP")
}

# Timeout value of previous response time in ticks
record(longin, "$(P)$(R)sspu_$(ID)_react_t_$(BRD)")
{
    field (DESC, "Previous Reaction Time")
    field(FLNK, "$(PRP)_trct_$(BRD) PP")
}
record(longin, "$(PRP)_trct_$(BRD)")
{
    field (DESC, "Reaction Time")
    field (EGU,  "ns")
    field (INP,  "$(P)$(R)sspu_$(ID)_react_t_$(BRD)")
    info(aa_policy, "default")
    info(archiver, "tn")
}

#############################################################
# State Supervision Param Route Register
#############################################################
record(longin, "$(P)$(R)#sspu_$(ID)_routes_$(BRD)")
{
    field(DESC, "Routing indexes in the fw")
    field(INP,  "$(P)$(R)#r_$(ID)_8192_0x007c_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#sspu_$(ID)_rout_fo_$(BRD)")
}

record(fanout, "$(P)$(R)#sspu_$(ID)_rout_fo_$(BRD)")
{
    field(DESC, "Routing indexes fanout")
    field(SELM, "All")
    field(LNK1, "$(P)$(R)#sspu_$(ID)_lnk_inx_$(BRD)")
    field(LNK2, "$(P)$(R)#sspu_$(ID)_lnk_orig_$(BRD)")
    field(LNK3, "$(P)$(R)#sspu_$(ID)_sgl_sinx_$(BRD)")
    field(LNK4, "$(P)$(R)#sspu_$(ID)_sgl_inx_$(BRD)")
}

record(calcout, "$(P)$(R)#sspu_$(ID)_lnk_inx_$(BRD)")
{
    field (DESC, "SLink index")
    field (INPA, "$(P)$(R)#sspu_$(ID)_routes_$(BRD).VAL")
    field (INPB, "0xf0000000")
    field (CALC, "(A & B) >> 28")
    field (OUT,  "$(P)$(R)sspu_$(ID)_lnk_inx_$(BRD) PP")
}

record(longin, "$(P)$(R)sspu_$(ID)_lnk_inx_$(BRD)")
{
    field (DESC, "SLink index")
}

record(calcout, "$(P)$(R)#sspu_$(ID)_lnk_orig_$(BRD)")
{
    field (DESC, "Slink origin")
    field (INPA, "$(P)$(R)#sspu_$(ID)_routes_$(BRD).VAL")
    field (INPB, "0x08000000")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)sspu_$(ID)_lnk_orig_$(BRD) PP")
}

record(bi, "$(P)$(R)sspu_$(ID)_lnk_orig_$(BRD)")
{
    field (DESC, "SLink origin")
    field (ZNAM, "Orig: fast channel")
    field (ONAM, "Orig: nested channel")
}

record(calcout, "$(P)$(R)#sspu_$(ID)_sgl_sinx_$(BRD)")
{
    field (DESC, "Signal SubIndex")
    field (INPA, "$(P)$(R)#sspu_$(ID)_routes_$(BRD).VAL")
    field (INPB, "0x03000000")
    field (CALC, "(A & B) >> 24")
    field (OUT,  "$(P)$(R)sspu_$(ID)_sgl_sinx_$(BRD) PP")
}

record(longin, "$(P)$(R)sspu_$(ID)_sgl_sinx_$(BRD)")
{
    field (DESC, "SLink SubIndex")
}

record(calcout, "$(P)$(R)#sspu_$(ID)_sgl_inx_$(BRD)")
{
    field (DESC, "Signal Index")
    field (INPA, "$(P)$(R)#sspu_$(ID)_routes_$(BRD).VAL")
    field (INPB, "0x00ffffff")
    field (CALC, "A & B")
    field (OUT,  "$(P)$(R)sspu_$(ID)_sgl_inx_$(BRD) PP")
}

record(longin, "$(P)$(R)sspu_$(ID)_sgl_inx_$(BRD)")
{
    field (DESC, "Signal Index")
}
