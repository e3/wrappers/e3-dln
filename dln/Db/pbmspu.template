# Proton Beam Mode (PBM) Signal Processing Unit
# ID        = ID the number of the SPU                              0,1,2,3, ...
# 2048      = C_DLN_CS_PBMSPU
# 3rd arg   = index of the register.                                pbmspid = 1, pbmspp = 2, ...

#############################################################
# PBM SPID
#############################################################
record(longin, "$(P)$(R)pbmspu_$(ID)_id_$(BRD)")
{
    field(DESC, "PBMSPU ID")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x0004_$(BRD).VAL CPP")
    field(FLNK, "$(PRP)_id_$(BRD) PP")
}
record(longin, "$(PRP)_id_$(BRD)")
{
    field (DESC, "PBMSPU id")
    field (INP,  "$(P)$(R)pbmspu_$(ID)_id_$(BRD)")
    info(aa_policy, "default")
    info(archiver, "tn")
}


record(stringin, "$(P)$(R)pbmspu_$(ID)_id_label_$(BRD)")
{
  field(DESC, "PBMSPU ID label")
  field(VAL, "$(LABEL)")
}

#############################################################
# PBD Signal Processing Param Register
#############################################################
record(longin, "$(P)$(R)#pbmspu_$(ID)_fm_stat_$(BRD)")
{
    field(DESC, "Filter and mask status")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x0008_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#pbmspu_$(ID)_fm_fo_$(BRD)")
}

record(fanout, "$(P)$(R)#pbmspu_$(ID)_fm_fo_$(BRD)")
{
    field(DESC, "Filter and mask status fanout")
    field(SELM, "All")
    field(LNK1, "$(P)$(R)#pbmspu_$(ID)_mask_en_$(BRD)")
    field(LNK2, "$(P)$(R)#pbmspu_$(ID)_filt_en_$(BRD)")
}

# Filter enable
record(calcout, "$(P)$(R)#pbmspu_$(ID)_filt_en_$(BRD)")
{
    field (DESC, "Filtering enable")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_fm_stat_$(BRD).VAL")
    field (INPB, "0x00000002")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_filt_en_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_filt_en_$(BRD)")
{
    field (DESC, "Filtering enable")
    field (ZNAM, "Filtering not enable")
    field (ONAM, "Filtering enable")
}

# Mask enable
record(calcout, "$(P)$(R)#pbmspu_$(ID)_mask_en_$(BRD)")
{
    field (DESC, "Masking enable")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_fm_stat_$(BRD).VAL")
    field (INPB, "0x00000001")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_mask_en_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_mask_en_$(BRD)")
{
    field (DESC, "Masking enable")
    field (ZNAM, "Mask not enable")
    field (ONAM, "Mask enable")
}

#############################################################
# PBD Signal Processing Status Word Register
#############################################################
record(longin, "$(P)$(R)#pbmspu_$(ID)_pu_stat_$(BRD)")
{
    field(DESC, "PBMSPU status")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x000c_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#pbmspu_$(ID)_stat_fo_$(BRD)")
}

record(fanout, "$(P)$(R)#pbmspu_$(ID)_stat_fo_$(BRD)")
{
    field(DESC, "PBMSPU status Fanout")
    field(SELM, "All")
    field(LNK1, "$(P)$(R)#pbmspu_$(ID)_in_calc_$(BRD)")
    field(LNK2, "$(P)$(R)#pbmspu_$(ID)_t_calc_$(BRD)")
    field(LNK3, "$(P)$(R)#pbmspu_$(ID)_dlv_ltch_$(BRD)")
    field(LNK4, "$(P)$(R)#pbmspu_$(ID)_dlv_ok_$(BRD)")
    field(LNK5, "$(P)$(R)#pbmspu_$(ID)_mask_val_$(BRD)")
    field(LNK6, "$(P)$(R)#pbmspu_$(ID)_flt_flag_$(BRD)")
}

# PBM Input (from SCU)
record(calcout, "$(P)$(R)#pbmspu_$(ID)_in_calc_$(BRD)")
{
    field (DESC, "Calc val PBM SPU input")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x000ff000")
    field (CALC, "(A & B) >> 12")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_in_bm_$(BRD) PP")
}

record(mbbi, "$(P)$(R)pbmspu_$(ID)_in_bm_$(BRD)")
{
    field (DESC, "Beam Mode at input")
    field (INP,  "$(P)$(R)#pbmspu_$(ID)_in_calc_$(BRD)")
    field (ZRST, "Error")
    field (ONST, "Undefined")
    field (TWST, "None")
    field (THST, "NoBeam")
    field (FRST, "Conditioning")
    field (FVST, "Probe")
    field (SXST, "FastCommissioning")
    field (SVST, "RfTest")
    field (EIST, "StabilityTest")
    field (NIST, "SlowCommissioning")
    field (TEST, "FastTuning")
    field (ELST, "SlowTuning")
    field (TVST, "LongPulseVerification")
    field (TTST, "ShieldingVerification")
    field (FTST, "Production")
    field (FLNK, "$(PRP)_in_$(BRD) PP")
}
record(mbbi, "$(PRP)_in_$(BRD)")
{
    field (DESC, "Beam Mode at input")
    field (INP,  "$(P)$(R)pbmspu_$(ID)_in_bm_$(BRD)")
    field (ZRST, "Error")
    field (ONST, "Undefined")
    field (TWST, "None")
    field (THST, "NoBeam")
    field (FRST, "Conditioning")
    field (FVST, "Probe")
    field (SXST, "FastCommissioning")
    field (SVST, "RfTest")
    field (EIST, "StabilityTest")
    field (NIST, "SlowCommissioning")
    field (TEST, "FastTuning")
    field (ELST, "SlowTuning")
    field (TVST, "LongPulseVerification")
    field (TTST, "ShieldingVerification")
    field (FTST, "Production")
    info(aa_policy, "default")
    info(archiver, "tn")
}

# Request PBM Input (from Timing System)
record(calcout, "$(P)$(R)#pbmspu_$(ID)_t_calc_$(BRD)")
{
    field (DESC, "Calc val PBM TS input")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000ff0")
    field (CALC, "(A & B) >> 4")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_time_bm_$(BRD) PP")
}

record(mbbi, "$(P)$(R)pbmspu_$(ID)_time_bm_$(BRD)")
{
    field (DESC, "Beam Mode from timing")
    field (INP,  "$(P)$(R)#pbmspu_$(ID)_t_calc_$(BRD)")
    field (ZRST, "NoBeam")
    field (ONST, "Conditioning")
    field (TWST, "Probe")
    field (THST, "FastCommissioning")
    field (FRST, "RfTest")
    field (FVST, "StabilityTest")
    field (SXST, "SlowCommissioning")
    field (SVST, "FastTuning")
    field (EIST, "SlowTuning")
    field (NIST, "LongPulseVerification")
    field (TEST, "ShieldingVerification")
    field (ELST, "Production")
}

## Check if PBM from sensor and timing system are the same
record(calcout, "$(P)$(R)#pbmspu_$(ID)_in_ok_$(BRD)")
{
    field (DESC, "Are the two PBM ok?")
    field (INPA, "$(P)$(R)pbmspu_$(ID)_in_bm_$(BRD).VAL CPP")
    field (INPB, "$(P)$(R)pbmspu_$(ID)_time_bm_$(BRD).VAL CPP")
    field (CALC, "(A = (B+3)) ? 1 : 0")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_in_ok_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_in_ok_$(BRD)")
{
    alias("$(PRP)_in_ok_$(BRD)")
    field (DESC, "Is the input ok?")
    field (ZNAM, "Input or timing not OK")
    field (ONAM, "Input and timing OK")
    field (ZSV,  "MINOR")
    field (OSV,  "NO_ALARM")
}

# DLV Latched
record(calcout, "$(P)$(R)#pbmspu_$(ID)_dlv_ltch_$(BRD)")
{
    field (DESC, "DLV Latched")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000008")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_dlv_ltch_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_dlv_ltch_$(BRD)")
{
    field (DESC, "DLV Latched ")
    field (ZNAM, "DLN not latched")
    field (ONAM, "DLV latched")
    field (ZSV,  "MAJOR")
    field (OSV,  "NO_ALARM")
}

# constant to 1. It can never been latched. Needed for e3-fbis
record(bi, "$(PRP)_lch_n_$(BRD)")
{
    field (DESC, "Need for e3-fbis")
    field (ZNAM, "DLN not not latched")
    field (ONAM, "DLV not latched")
    field (VAL, "1")
    field (PINI, "1")
}

# DLV OK
record(calcout, "$(P)$(R)#pbmspu_$(ID)_dlv_ok_$(BRD)")
{
    field (DESC, "DLV OK")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000004")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_dlv_ok_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_dlv_ok_$(BRD)")
{
    field (DESC, "DLV OK")
    field (ZNAM, "DLV not OK")
    field (ONAM, "DLV OK")
    field (ZSV,  "MAJOR")
    field (OSV,  "NO_ALARM")
    field (FLNK, "$(PRP)_$(BRD) PP")
}
record(bi, "$(PRP)_$(BRD)")
{
    field (DESC, "DLV OK")
    field (INP,  "$(P)$(R)pbmspu_$(ID)_dlv_ok_$(BRD)")
    field (ZNAM, "DLN NOK")
    field (ONAM, "DLV OK")
    field (ZSV,  "MAJOR")
    field (OSV,  "NO_ALARM")
    info(aa_policy, "default")
    info(archiver, "tn")
}

#Masked flag
record(calcout, "$(P)$(R)#pbmspu_$(ID)_mask_val_$(BRD)")
{
    field (DESC, "Masked applied")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000002")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_mask_val_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_mask_val_$(BRD)")
{
    field (DESC, "Masked applied")
    field (ZNAM, "Mask not applied")
    field (ONAM, "Mask applied")
    field (FLNK, "$(PRP)_mask_$(BRD) PP")
}
record(bi, "$(PRP)_mask_$(BRD)")
{
    field (DESC, "Masked applied")
    field (INP,  "$(P)$(R)pbmspu_$(ID)_mask_val_$(BRD)")
    field (ZNAM, "Mask not applied")
    field (ONAM, "Mask applied")
    info(aa_policy, "default")
    info(archiver, "tn")
}

#Filter Flag
record(calcout, "$(P)$(R)#pbmspu_$(ID)_flt_flag_$(BRD)")
{
    field (DESC, "Filtering applied")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_pu_stat_$(BRD).VAL")
    field (INPB, "0x00000001")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_flt_flag_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_flt_flag_$(BRD)")
{
    field (DESC, "Filtering applied")
    field (ZNAM, "Filtering not apllied")
    field (ONAM, "Filtering applied")
    field (FLNK, "$(PRP)_flt_$(BRD) PP")
}
record(bi, "$(PRP)_flt_$(BRD)")
{
    field (DESC, "Filter flag")
    field (INP,  "$(P)$(R)pbmspu_$(ID)_flt_flag_$(BRD)")
    field (ZNAM, "Filtering not applied")
    field (ONAM, "Filtering applied")
    info(aa_policy, "default")
    info(archiver, "tn")
}

#############################################################
# PBM Signal Processing Mask Mode Status/Control Register
#############################################################
record(longin, "$(P)$(R)#pbmspu_$(ID)_msk_typ_$(BRD)")
{
    field(DESC, "PBMSPU Mask Type")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x0010_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#pbmspu_$(ID)_c_m_typ_$(BRD)")
}

record(calcout, "$(P)$(R)#pbmspu_$(ID)_c_m_typ_$(BRD)")
{
    field(DESC, "PBMSPU calc Mask Type")
    field(INPA, "$(P)$(R)#pbmspu_$(ID)_msk_typ_$(BRD).VAL")
    field(INPB, "0x0000000f")
    field(CALC, "A & B")
    field(OUT,  "$(P)$(R)pbmspu_$(ID)_msk_typ_$(BRD) PP" )
}

# Type of mask
record(mbbi, "$(P)$(R)pbmspu_$(ID)_msk_typ_$(BRD)")
{
    field (DESC, "PBMSPU Mask Type")
    field (INP,  "$(P)$(R)#pbmspu_$(ID)_c_m_typ_$(BRD)")
    field (ONST, "Not masked")
    field (TWST, "Masked")
}

## WRITE
record(mbbo, "$(P)$(R)pbmspu_$(ID)_set_msk_$(BRD)")
{
    field (DESC, "Set Mask Mode")
    field (DTYP, "Soft Channel")
    field (ZRST, "Not masked")
    field (ONST, "Masked")
    field (FLNK, "$(P)$(R)#pbmspu_$(ID)_c_set_m_$(BRD)")
    field(ASG,"mps-critical")
    info (autosaveFields, "VAL")
}

record(calcout, "$(P)$(R)#pbmspu_$(ID)_c_set_m_$(BRD)")
{
    field (DESC, "Set Mask Mode")
    field (INPA, "$(P)$(R)pbmspu_$(ID)_set_msk_$(BRD)")
    field (CALC, "2**A & 0x000f")
    field (OUT,  "$(P)$(R)#pbmspu_$(ID)_set_msk_$(BRD) PP")
}

record(longout, "$(P)$(R)#pbmspu_$(ID)_set_msk_$(BRD)")
{
    alias ("$(PRP)_sm_$(BRD)")
    field (DESC, "Set Mask Mode")
    field (DTYP, "stream")
    field (OUT,  "@dln.proto write(0x08,$(ID),0x11,0x90) $(PORT)")
}

#############################################################
# PBD Signal Processing Mask Param PBM Register
#############################################################
record(longin, "$(P)$(R)#pbmspu_$(ID)_pbm_mk_$(BRD)")
{
    field(DESC, "PBM that can be masked")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x0014_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#pbmspu_$(ID)_pbm_msk_$(BRD)")
}

# pbmspu mask modes
record(calcout, "$(P)$(R)#pbmspu_$(ID)_pbm_msk_$(BRD)")
{
    field (DESC, "PBM that can be masked")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_pbm_mk_$(BRD).VAL")
    field (INPB, "0x00001fff")
    field (CALC, "A & B")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_pbm_msk_$(BRD) PP")
}

record(longin, "$(P)$(R)pbmspu_$(ID)_pbm_msk_$(BRD)")
{
    field (DESC, "PBM that can be masked")
}

#############################################################
# PBD Signal Processing Mask Param PBD Register
#############################################################
record(longin, "$(P)$(R)#pbmspu_$(ID)_pbd_mk_$(BRD)")
{
    field(DESC, "PBD that can be masked")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x0018_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#pbmspu_$(ID)_pbd_msk_$(BRD)")
}

# pbmspu mask desinations
record(calcout, "$(P)$(R)#pbmspu_$(ID)_pbd_msk_$(BRD)")
{
    field (DESC, "PBD that can be masked")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_pbd_mk_$(BRD).VAL")
    field (INPB, "0x000003ff")
    field (CALC, "A & B")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_pbd_msk_$(BRD) PP")
}

record(longin, "$(P)$(R)pbmspu_$(ID)_pbd_msk_$(BRD)")
{
    field (DESC, "PBM that can be masked")
}

#############################################################
# PBD Signal Processing Masking Conditions Status Register
#############################################################
record(longin, "$(P)$(R)#pbmspu_$(ID)_msk_met_$(BRD)")
{
    field(DESC, "Mask match the PBM and PBD")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x001c_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#pbmspu_$(ID)_m_mt_fo_$(BRD)")
}

record(fanout, "$(P)$(R)#pbmspu_$(ID)_m_mt_fo_$(BRD)")
{
    field(DESC, "Mask match the PBM and PBD")
    field(SELM, "All")
    field(LNK1, "$(P)$(R)#pbmspu_$(ID)_bd_m_met_$(BRD)")
    field(LNK2, "$(P)$(R)#pbmspu_$(ID)_bm_m_met_$(BRD)")
}

# PBD Masking conditions met
record(calcout, "$(P)$(R)#pbmspu_$(ID)_bd_m_met_$(BRD)")
{
    field (DESC, "Mask match the PBD")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_msk_met_$(BRD).VAL CPP")
    field (INPB, "0x00000002")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_bd_m_met_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_bd_m_met_$(BRD)")
{
    field (DESC, "Mask match the PBD")
    field (ZNAM, "PBD mask not met")
    field (ONAM, "PBD mask met")
    field (ZSV,  "MINOR")
    field (OSV,  "NO_ALARM")
}

# PBM Masking conditions met
record(calcout, "$(P)$(R)#pbmspu_$(ID)_bm_m_met_$(BRD)")
{
    field (DESC, "Mask match the PBD")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_msk_met_$(BRD).VAL CPP")
    field (INPB, "0x00000001")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_bm_m_met_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_bm_m_met_$(BRD)")
{
    field (DESC, "Mask match the PBM")
    field (ZNAM, "PBM mask not met")
    field (ONAM, "PBM mask met")
    field (ZSV,  "MINOR")
    field (OSV,  "NO_ALARM")
}


#############################################################
# PBD Signal Processing Filter Status Register
#############################################################
record(longin, "$(P)$(R)#pbmspu_$(ID)_flt_met_$(BRD)")
{
    field(DESC, "Filter match the PBM and PBD")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x0028_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#pbmspu_$(ID)_spfs_fanout_$(BRD)")
}

record(fanout, "$(P)$(R)#pbmspu_$(ID)_spfs_fanout_$(BRD)")
{
    field(DESC, "Filter match the PBM and PBD")
    field(SELM, "All")
    field(LNK1, "$(P)$(R)#pbmspu_$(ID)_dm_f_met_$(BRD)")
    field(LNK2, "$(P)$(R)#pbmspu_$(ID)_d_fl_met_$(BRD)")
    field(LNK3, "$(P)$(R)#pbmspu_$(ID)_m_fl_met_$(BRD)")
}

# Filter flag which is the coupling of the of the PBD filter and PBM filter flags
record(calcout, "$(P)$(R)#pbmspu_$(ID)_dm_f_met_$(BRD)")
{
    field (DESC, "Filter match the PBM and PBD")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_flt_met_$(BRD).VAL")
    field (INPB, "0x00000004")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_dm_f_met_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_dm_f_met_$(BRD)")
{
    field (DESC, "Filter match the PBM and PBD")
    field (ZNAM, "PBM and PBD not matched")
    field (ONAM, "PBM and PBD matched")
    field (ZSV,  "MINOR")
    field (OSV,  "NO_ALARM")
}

#PBM filter flag is set when the requested Proton Beam Destination matches the
#parametrized PBM mask
record(calcout, "$(P)$(R)#pbmspu_$(ID)_d_fl_met_$(BRD)")
{
    field (DESC, "Filter match the PBD")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_flt_met_$(BRD).VAL")
    field (INPB, "0x00000002")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_d_fl_met_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_d_fl_met_$(BRD)")
{
    field (DESC, "Filter match the PBD")
    field (ZNAM, "PBD filter not matched")
    field (ONAM, "PBD filter matched")
    field (ZSV,  "MINOR")
    field (OSV,  "NO_ALARM")
}

#PBD filter flag is set when the requested Proton Beam Mode matches the
#parametrized PBD mask
record(calcout, "$(P)$(R)#pbmspu_$(ID)_m_fl_met_$(BRD)")
{
    field (DESC, "Filter match the PBM")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_flt_met_$(BRD).VAL")
    field (INPB, "0x00000001")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_m_fl_met_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_m_fl_met_$(BRD)")
{
    field (DESC, "Filter match the PBM")
    field (ZNAM, "PBM filter not matched")
    field (ONAM, "PBM filter matched")
    field (ZSV,  "MINOR")
    field (OSV,  "NO_ALARM")
}

#############################################################
# PBD Signal Processing Filter Param PBM Register
#############################################################
record(longin, "$(P)$(R)#pbmspu_$(ID)_pbm_filt_$(BRD)")
{
    field(DESC, "PBM that can be filtered")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x002c_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#pbmspu_$(ID)_pbm_flt_$(BRD)")
}

# pbmspu filter modes
record(calcout, "$(P)$(R)#pbmspu_$(ID)_pbm_flt_$(BRD)")
{
    field (DESC, "PBM that can be filtered")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_pbm_filt_$(BRD).VAL")
    field (INPB, "0x00001fff")
    field (CALC, "A & B")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_pbm_flt_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_pbm_flt_$(BRD)")
{
    field (DESC, "PBM that can be filtered")
}

#############################################################
# PBD Signal Processing Filter Param PBD Register
#############################################################
record(longin, "$(P)$(R)#pbmspu_$(ID)_pbd_filt_$(BRD)")
{
    field(DESC, "PBD that can be filtered")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x0030_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#pbmspu_$(ID)_pbd_flt_$(BRD)")
}

# pbmspu filter desinations
record(calcout, "$(P)$(R)#pbmspu_$(ID)_pbd_flt_$(BRD)")
{
    field (DESC, "PBD that can be filtered")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_pbd_filt_$(BRD).VAL")
    field (INPB, "0x000003ff")
    field (CALC, "A & B")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_pbd_flt_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_pbd_flt_$(BRD)")
{
    field (DESC, "PBD that can be filtered")
}

#############################################################
# PBD Signal Processing Param Route Register
#############################################################
record(longin, "$(P)$(R)#pbmspu_$(ID)_routes_$(BRD)")
{
    field(DESC, "Routing indexes in the fw")
    field(INP,  "$(P)$(R)#r_$(ID)_2048_0x007c_$(BRD).VAL CPP")
    field(FLNK, "$(P)$(R)#pbmspu_$(ID)_rout_fo_$(BRD)")
}

record(fanout, "$(P)$(R)#pbmspu_$(ID)_rout_fo_$(BRD)")
{
    field(DESC, "Routing indexes fanout")
    field(SELM, "All")
    field(LNK1, "$(P)$(R)#pbmspu_$(ID)_lnk_inx_$(BRD)")
    field(LNK2, "$(P)$(R)#pbmspu_$(ID)_lnk_orig_$(BRD)")
    field(LNK3, "$(P)$(R)#pbmspu_$(ID)_sgl_sinx_$(BRD)")
    field(LNK4, "$(P)$(R)#pbmspu_$(ID)_sgl_inx_$(BRD)")
}

record(calcout, "$(P)$(R)#pbmspu_$(ID)_lnk_inx_$(BRD)")
{
    field (DESC, "SLink index")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_routes_$(BRD).VAL")
    field (INPB, "0xf0000000")
    field (CALC, "(A & B) >> 28")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_lnk_inx_$(BRD) PP")
}

record(longin, "$(P)$(R)pbmspu_$(ID)_lnk_inx_$(BRD)")
{
    field (DESC, "SLink index")
}

record(calcout, "$(P)$(R)#pbmspu_$(ID)_lnk_orig_$(BRD)")
{
    field (DESC, "Slink origin")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_routes_$(BRD).VAL")
    field (INPB, "0x08000000")
    field (CALC, "(A & B) = 0 ? 0 : 1")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_lnk_orig_$(BRD) PP")
}

record(bi, "$(P)$(R)pbmspu_$(ID)_lnk_orig_$(BRD)")
{
    field (DESC, "SLink origin")
    field (ZNAM, "Orig: fast channel")
    field (ONAM, "Orig: nested channel")
}

record(calcout, "$(P)$(R)#pbmspu_$(ID)_sgl_sinx_$(BRD)")
{
    field (DESC, "Signal SubIndex")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_routes_$(BRD).VAL")
    field (INPB, "0x03000000")
    field (CALC, "(A & B) >> 24")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_sgl_sinx_$(BRD) PP")
}

record(longin, "$(P)$(R)pbmspu_$(ID)_sgl_sinx_$(BRD)")
{
    field (DESC, "SLink SubIndex")
}

record(calcout, "$(P)$(R)#pbmspu_$(ID)_sgl_inx_$(BRD)")
{
    field (DESC, "Signal Index")
    field (INPA, "$(P)$(R)#pbmspu_$(ID)_routes_$(BRD).VAL")
    field (INPB, "0x00ffffff")
    field (CALC, "A & B")
    field (OUT,  "$(P)$(R)pbmspu_$(ID)_sgl_inx_$(BRD) PP")
}

record(longin, "$(P)$(R)pbmspu_$(ID)_sgl_inx_$(BRD)")
{
    field (DESC, "Signal Index")
}
