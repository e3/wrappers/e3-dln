#include <stdio.h>
#include <inttypes.h>
#include <math.h>
#include <strings.h>
#include <dbDefs.h>
#include <subRecord.h>
#include <dbCommon.h>
#include <recSup.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <epicsTypes.h>
#include <cantProceed.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <aSubRecord.h>
#include <epicsStdlib.h>
#include <epicsStdio.h>
#include <locale.h>

#include <string.h> // Provides memcpy prototype
#include <stdlib.h> // Provides calloc prototype
#include <unistd.h> // to provide sleep function

#include <time.h>

static long mode_dec_set(struct subRecord *psub) {

    epicsInt32 a;
    a = (epicsInt32)psub->a;


	switch(a) {
		case   0:
			psub->val = 2;
			break;
		case  1:
			psub->val = 3;
			break;
		case  10:
			psub->val = 4;
			break;
		case  20:
			psub->val = 5;
			break;
		case  30:
			psub->val = 6;
			break;
		case  40:
			psub->val = 7;
			break;
		case  50:
			psub->val = 8;
			break;
		case  60:
			psub->val = 9;
			break;
		case  70:
			psub->val = 10;
			break;
		case  80:
			psub->val = 11;
			break;
		case  90:
			psub->val = 12;
			break;
		case  100:
			psub->val = 13;
			break;
		case  110:
			psub->val = 14;
			break;
		case  254:
			psub->val = 1;
			break;
		case  255:
			psub->val = 0;
			break;
		default:
			psub->val = 0;
			break;
	}

    return 0;
}

static long dest_dec_set(struct subRecord *psub) {

    epicsInt32 a;
    a = (epicsInt32)psub->a;

	switch(a) {
		case   0:
			psub->val = 2;
			break;
		case  10:
			psub->val = 3;
			break;
		case  20:
			psub->val = 4;
			break;
		case  30:
			psub->val = 5;
			break;
		case  40:
			psub->val = 6;
			break;
		case  50:
			psub->val = 7;
			break;
		case  60:
			psub->val = 8;
			break;
		case  70:
			psub->val = 9;
			break;
		case  80:
			psub->val = 10;
			break;
		case  90:
			psub->val = 11;
			break;
		case  254:
			psub->val = 1;
			break;
		case  255:
			psub->val = 0;
			break;
		default:
			psub->val = 0;
			break;
	}
    return 0;
}

static long sub_regs(aSubRecord *prec)
{
    uint32_t *b = (uint32_t*)prec->b;

	uint32_t sevr = b[0];
	uint32_t n = prec->noa;

	if (sevr == 0 )
       memcpy(prec->vala, (uint32_t*)prec->a, n * sizeof(uint32_t));
	else
	   memset(prec->vala, 0, n * sizeof(uint32_t));

    return 0;
}

static long two_s_complement(aSubRecord *prec)
{
    uint32_t *a = (uint32_t*)prec->a;
    int32_t signed_val = (int32_t)a[0];
	char  val[12];

	sprintf(val, "%u", a[0]);
	memcpy(prec->vala, val, 12 * sizeof(char));

    return 0;
}


static int epoch_time_asub(aSubRecord *precord) {
    char outcalc[40];
    unsigned long nelements_vala;

    nelements_vala = precord->nova;

    uint32_t *a = (uint32_t*)precord->a;

    time_t rawtime = a[0];
    struct tm  ts;
    char       buf[40];

    // Format time, "ddd yyyy-mm-dd hh:mm:ss zzz"
    ts = *localtime(&rawtime);
    strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", &ts);

    strcpy(outcalc, buf);

    memcpy(precord->vala, outcalc, 40 * sizeof(char));

    precord->neva = nelements_vala;
    return 0;
}

void get_dln_details(int sup, int t, int sub, int det, char * c_sup, char * c_t, char * c_sub, char * c_d) {

	if (sup == 1){
        strcpy(c_sup, "PU input");
	}
	else if (sup == 2){
		strcpy(c_sup, "PU DLV out");
	}
	else if (sup == 3){
		strcpy(c_sup, "PU DLV latched");
	}
	else if (sup == 4){
		strcpy(c_sup, "BSO");
	}
	else
		strcpy(c_sup, " ");

        if (sup == 1 || sup == 2 || sup ==3) { // If these supertypes then the types mean ::
		if (t ==  1) {
			strcpy(c_t, "SPU");}
		else if (t == 2) {
			strcpy(c_t, "PBDSPU");}
		else if (t == 3) {
			strcpy(c_t, "PBMSPU");}
		else if (t == 5) {
			strcpy(c_t, "SSPU");}
		else
			strcpy(c_t, " ");
	}
	else if (sup == 4) {
		strcpy(c_d, " ");
		if (t ==  1) {
			strcpy(c_t, "GBP");
			if  (sub == 2) strcpy(c_sub, "NOK");
			else strcpy(c_sub, "OK");
		}
		else if (t == 2) {
			strcpy(c_t, "Local BI");
			if  (sub == 2) strcpy(c_sub, "OK");
			else strcpy(c_sub, "NOK");
		}
		else if (t == 3) {
			strcpy(c_t, "Local RBI");
			if  (sub == 2) strcpy(c_sub, "OK");
			else strcpy(c_sub, "NOK");
		}
		else if (t == 4) {
			strcpy(c_t, "Local EBI");
			if  (sub == 2) strcpy(c_sub, "OK");
			else strcpy(c_sub, "NOK");
		}
		else if (t == 5) {
			strcpy(c_t, "OPL");
			if  (sub == 2) {
				strcpy(c_sub, "Down");
			}
			else {
				strcpy(c_sub, "Up");
			}
			if      (det == 1) strcpy(c_d, "OK");
			else if (det == 2) strcpy(c_d, "BI");
			else if (det == 4) strcpy(c_d, "RBI");
			else               strcpy(c_d, "EBI");
		}
		else if (t == 6) {
			strcpy(c_t, "PM");
			if  (sub == 2) strcpy(c_sub, "");
			else strcpy(c_sub, "");
		}
		else
			strcpy(c_t, " ");
	}
	else
		strcpy(c_t, " ");

        if (sup == 1){ // PU input

		if (t ==1 || t == 5 ){
			if  (det == 2) strcpy(c_d, "OK");
			else if (det == 1) strcpy(c_d, "NOK");
			else strcpy(c_d, "ERR");
		}
		else if (t == 2){
			if      (det == 0) strcpy(c_d, "ERR");
			else if (det == 2) strcpy(c_d, "None");
			else if (det == 3) strcpy(c_d, "Isrc");
			else if (det == 4) strcpy(c_d, "LebtFc");
			else if (det == 5) strcpy(c_d, "MebtFc");
			else if (det == 6) strcpy(c_d, "Dtl2Fc");
			else if (det == 7) strcpy(c_d, "Dtl4Fc");
			else if (det == 8) strcpy(c_d, "SpkIbs");
			else if (det == 9) strcpy(c_d, "MblIbs");
			else if (det ==10) strcpy(c_d, "BeamDump");
			else if (det ==11) strcpy(c_d, "Target");
			else strcpy(c_d, "Undefined");
		}
		else if (t == 3){
			if      (det == 0) strcpy(c_d, "ERR");
			else if (det == 2) strcpy(c_d, "None");
			else if (det == 3) strcpy(c_d, "Nobeam");
			else if (det == 4) strcpy(c_d, "Conditioning");
			else if (det == 5) strcpy(c_d, "Probe");
			else if (det == 6) strcpy(c_d, "FastComm.");
			else if (det == 7) strcpy(c_d, "RfTest");
			else if (det == 8) strcpy(c_d, "Stability");
			else if (det == 9) strcpy(c_d, "SlowCom.");
			else if (det ==10) strcpy(c_d, "FastTun.");
			else if (det ==11) strcpy(c_d, "SlowTun.");
			else if (det ==12) strcpy(c_d, "LongPulsV.");
			else if (det ==13) strcpy(c_d, "ShieldVer.");
			else if (det ==14) strcpy(c_d, "Production");
			else strcpy(c_d, "Undefined");
		}

		else
			strcpy(c_d, " ");

                if (sub == 0)
        	    sprintf(c_sub, "%d", sub);
		else
		    sprintf(c_sub, "%d", sub-1);
	}

    else if (sup == 2){ // PU DLV OK

		if (t ==1 || t >= 4 ){
			if  ((det & 0x1) != 0) strcpy(c_d, "OK");
			else strcpy(c_d, "NOK");
		}
		else if (t == 2 || t == 3){
			if  (det == 0) strcpy(c_d, "NOK");
			else strcpy(c_d, "OK");
		}
		else
			strcpy(c_d, " ");

		if (t == 1 && sub == 1)      strcpy(c_sub, "SW BI");
		else if (t == 1 && sub == 2) strcpy(c_sub, "SW RBI");
		else if (t ==1)              sprintf(c_sub,"%d",sub-3);
		else if (t >= 4 && sub == 1) strcpy(c_sub, "SW EBI");
		else if (sub == 1)           strcpy(c_sub, "SW");
		else                         sprintf(c_sub, "%d", sub-2);
	}

	else if (sup == 3){ // PU latched
		if (t == 1  || t == 5){
			if  ((det & 0x2) != 0)
			   strcpy(c_d, "Latched");
			else
			   strcpy(c_d, "Not latched");
		}

		if (t == 1 && sub == 1)      strcpy(c_sub, "SW BI");
		else if (t == 1 && sub == 2) strcpy(c_sub, "SW RBI");
		else if (t == 1 && sub == 3) strcpy(c_sub, "SIS BI");
		else if (t == 1 && sub == 4) strcpy(c_sub, "SIS RBI");
		else if (t ==1)              sprintf(c_sub, "%d", sub-5);
		else if (sub == 1)           strcpy(c_sub, "SW");
		else                         sprintf(c_sub, "%d", sub-2);
	}
    else if (sup ==4 ) ;
	else
		strcpy(c_d, " ");


}

static int type_sub_det(aSubRecord *precord) {
    char  outA[40]; // supertype
    char  outB[40]; // type
	char  outC[40]; // type
	char  outD[40]; // details

    uint32_t *a = (uint32_t*)precord->a;
    uint32_t *b = (uint32_t*)precord->b;
	uint32_t *c = (uint32_t*)precord->c;

	int supertype = (b[0] & 0x0003f000)>>12;
	int type      = (b[0] & 0x00000f00)>>8;
	int subtype   = b[0] & 0x000000ff;
	int details   = c[0];

	//printf("supertype = %08x, type = %08x, subtype = %08x details = %08x\n", supertype, type, subtype, details);


    get_dln_details(supertype,type, subtype, details, outA, outB, outC, outD);

    memcpy(precord->vala, outA, 40 * sizeof(char));
    memcpy(precord->valb, outB, 40 * sizeof(char));
	memcpy(precord->valc, outC, 40 * sizeof(char));
	memcpy(precord->vald, outD, 40 * sizeof(char));

    char  vale[20];
    setlocale(LC_NUMERIC, "");
    sprintf(vale, "%'.1f", (((a[0] & 0xfffffff0)>>4)*8.192));
    memcpy(precord->vale, vale, 20 * sizeof(char));

	*(uint32_t *)precord->valf = b[0] & 0x80000000;
	*(uint32_t *)precord->valg = b[0] & 0x40000000;
	*(uint32_t *)precord->valh = b[0] & 0x20000000;
	*(uint32_t *)precord->vali = b[0] & 0x10000000;

   return 0;
}

static long set_epoch_time(struct subRecord *psub) {

	psub->val = (int)time(NULL);

    return(0);
}


epicsRegisterFunction(two_s_complement);

epicsRegisterFunction(mode_dec_set);
epicsRegisterFunction(dest_dec_set);

epicsRegisterFunction(sub_regs);

epicsRegisterFunction(type_sub_det);
epicsRegisterFunction(epoch_time_asub);

epicsRegisterFunction(set_epoch_time);
