
# EPICS for DLNs in FBIS

This document describes the design of the EPICS framework designed and implemented for monitoring and controlling the DLNs.

## Purpose

This document shall enable the reader to understand how the interface between EPICS and the DLNs work and how was implemented.

## Acronyms and Definitions
| Acronym   | Definition |
|-----------|------------|
| __CS__    | Chip Select |
| __DAOT__  | DLN Abstraction Over Tosca |
| __DLN__   | Decision Logic Node |
| __FBIS__  | Fast Beam Interlock System |
| __EBISPU__   | Emergency Beam Interlock Signal Processing Unit |
| __PBDSPU__   | Proton Beam Destination Signal Processing Unit |
| __PDMSPU__   | Proton Beam Mode Signal Processing Unit |
| __PU__   | Processing Unit |
| __SPU__   | Signal Processing Unit |
| __SSPU__   | Supervision Signal Processing Unit |


In the DLN firmware:
* a *SPU* is the unit to process OK/NOK signals. Typically used for beam permit and ready signals from Sensor Systems
* an *EBISPU* is the unit to process OK/NOK signals. It can be used to directly escalate to an Emergency Beam Interlock. Theoretically this SPU should never be used.
* a *PBDSPU* is the unit to process the PBD signals;
* a *PBMSPU* is the unit to process the PBM signals;
* a *SSPU* is the unit to process OK/NOK signals. Typically used for status and feedback signals from Actuator Systems.

PU is referred to all these types of processing units.

# DLN Hardware

In this context the DLN hardware is referred to the IFC1410 board. In this board there is a CPU and a FPGA.
* in the FPGA there is the DLN firmware, where basically all the DLN functionalities are implemented.
* in the CPU a daemon, called DAOT, runs. This program is a utility which provides an interface for high level programs to interact with
the DLN firmware configuration registers. The daemon can then  be reached on port 42000. An EPICS IOC can now communicate with the daot daemon over a TCP connection.

## Parametrization file (few words)
This file is an excel file. With this file for each segment formed by one DLN and its  SCUs, you can choose how the SCUs are populated (which MCs and in which slot), you can choose if an input (a beam permit, a ready, a proton beam destination, ...) in the DLN firmware is processed by an SPU, and SSPU an PBDSPU and so on. Then for each processing unit you can choose if it is maskable/filterable and latchable and also for which Proton Beam Destination and Mode.

More exhaustive information are here. https://confluence.esss.lu.se/display/PS/FBIS+documentation+from+ZHAW?preview=/332147079/349604432/FBIS_Parametrization.pdf

From the parametrization file you get a text file (.cfg or .ini).
These files are stored in the git repo https://gitlab.esss.lu.se/ics-protection/fbis/parametrization-files


## DLN Registers

The registers are segmented into sets of 32 register (see image below). A list of all registers available can be found in the pdf file in *docs* folder.
The current active register-set is controlled by the *Chip Select* register located at address 0x0.

![dln_regs](../docs/img/dln_regs.png "dln_regs")

The types of CS are:

![cs](../docs/img/cs.png "cs")

### DLN Registers access

In order to read/write to the register chosen, you have to:
1. Write the Chip Select register, according to the table above.
2. Read/write data from/to the desired register, knowing its address.

For example if you want to know if a system input (let's say it is the 8th input) is can be filtered,masked and latched; you have to:
1. set the CS to *C_DLN_CS_SPU* = 1024 and then adding the input number (8). So CS = 1024+8 = 1032;
2. read the register at the address 0x0008 that is the processing parameter register.

Another example could be, if you want to know the number of SPUs instantiated in the firmware. What you have to do is:
1. set the CS to *C_DLN_CS_CFG* = 0; CS = 0;
2. read the register at the address 0x000c, where it is stored the number of signal processing unit instances.


# System setup in lab

In the server room there is a DLN and an SCU connected to it.

The DLN configurations can change (a lot) for different DLNs and for the same DLN between different commissioning phases.

In the DLN crate, in the server room, there is one ifc1410 board, its RTM, an MCH and an CCPU.
In the CPU mounted on the ifc1410 board is running the DAOT, instead of the EPICS IOC is running in the CCPU.

# Few considerations about decisions taken.

The communication between the EPICS IOC and the DAOT is based on StreamDevice.
In the firmware used for test, there are about 1200 registers. To read or write a register you have to pass through the DAOT(https://gitlab.esss.lu.se/ics-protection/fbis/fbis_epics/dln/daot).
Read, in the worst case, hundreds of single registers periodically it is not well supported by **StreamDevice**.
So I divided the registers and hence the PVs in two groups:
1. Registers to be read once, because they are registers which values never changes. For example if a beam ready, so an SPU in the firmware, can be maskable or not. This is decided in the parametrization file and then the firmware is generated accordingly.
2. Registers oriented to experts. For example registers to read the value of an input or the value of the input after that masking or filtering has been applied.

In this firmware there are roughly 40% of the registers to be read once, and 60% of the registers to be read periodically.

The DAOT has been modified. Before you could read/write one register at each time.

Now you can read a block of register belonging to one of the category above. The number of registers depends on the DLN configuration. So when you send from the EPICS IOC to the DAOT the command to read a block of registers you send also the number of processing units that you want to read. In the EPICS side it has been implemented as a waveform of DTYP Stream. Then each element of the waveform is taken and linked to the “meaningful” raw PV. One PV for each register.

For testing purpose the waveform reading the registers with “higher priorities” has a SCAN of 0.1 second.


The DAOT has been modified also to be able to rearm a whole DLN, that means to reset all the latched inputs that caused a regular beam interlock.

# A short description of the files in the IOC e3-dln
* In Db folder:
  * there is a template file for each type of the CS(Chip Select). There is one raw PV for every register described in each CS section (file FBIS_FW_DD.pdf Chapter 2). Then other PVs can exist to extract meaningful value;
  * the DLN0x_PVs.tempate. This file is generated with a python script from the parametrization file (actually the file .cfg). You  can see the two waveforms reading the three different groups of PVs and then a subArray PV for each element of the waveform records. These subArray PVs are linked to the PVs listed in the other template files.
  * The proto file. There are all the commands recognized by the DAOT.
* In the src folder:
  * there are the subroutines C snippets used by some PVs in .template files to encode/decode the values for proton beam destinations and beam modes.
* In the iocsh folder:
  * there is an iocsh file. This file is generated with a python script from the parametrization file (actually the file .cfg).  Basically here, depending on the number and on the type of the processing units instantiated in the firmware, we load the same number and “type” of the template files.
